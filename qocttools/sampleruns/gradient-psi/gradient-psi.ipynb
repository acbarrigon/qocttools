{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Check on the accuracy of the calculation of the QOCT gradients"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    get_ipython\n",
    "    isnotebook = True\n",
    "except:\n",
    "    isnotebook = False\n",
    "\n",
    "import os\n",
    "import sys\n",
    "import numpy as np\n",
    "import scipy as sp\n",
    "import matplotlib\n",
    "if not isnotebook:\n",
    "    matplotlib.use('Agg')\n",
    "import matplotlib.pyplot as plt\n",
    "import time\n",
    "import qutip as qt\n",
    "from qutip_qip.operations import toffoli"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.models.GdW30 as GdW30\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.math_extra as math_extra\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.solvers as solvers\n",
    "import qocttools.target as target"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In order to calculate the optimal pulse that induces a given reaction in a quantum system, one defines a function of that pulse that must be optimized. One important ingredient for the optimization is derivative of this function with respect to the control parameters that define the pulse. In this script we check that this gradient or derivative is calculated correctly."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is defined by the Hamiltonian:\n",
    "\n",
    "\\begin{equation}\n",
    "        \\hat{H}(t) = \\hat{H}_0 + f(t)\\hat{V}\n",
    "\\end{equation}\n",
    "where the time-independent part is given by:\n",
    "\\begin{equation}\n",
    "        \\hat{H}_0 = D\\bigg[\\hat{S}_z^2 - \\frac{1}{3}S(S + 1)\\bigg] + E[\\hat{S}_x^2 - \\hat{S}_y^2] - g\\mu_B\\hat{\\vec{S}}\\cdot\\vec{H}\n",
    "\\end{equation}\n",
    "and the time-dependent part is:\n",
    "\\begin{equation}\n",
    "        \\hat{H}(t) = \\hat{H}_0 + f(t)\\hat{V}\n",
    "\\end{equation}\n",
    "The perturbation is a magnetic field:\n",
    "\\begin{equation}\n",
    "        \\hat{V} = -g\\mu_B\\hat{\\vec{S}}\\cdot\\vec{H}_m \n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case:\n",
    "\n",
    "* $S = 7/2$\n",
    "\n",
    "* $D$ = 1281 MHz\n",
    "\n",
    "* $E$ = 294 MHz\n",
    "\n",
    "* $\\vec{H} = (0.15, 0.0, 0.0)$ T\n",
    "\n",
    "* $\\vec{H}_m = (0, 0.001, 0.0)$ T"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "S = 7/2 # spin\n",
    "E = 294 # value in MHz\n",
    "D = 1281 # value in MHz\n",
    "dim = int(2*S + 1) #matrix dim\n",
    "\n",
    "# The target is the population of the first excited state.\n",
    "target_level = 1"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = np.array([0.15, 0, 0.0], dtype = float) #magnetic field in T\n",
    "H_m = np.array([0, 0.001, 0], dtype = float) #only in presence of perturbation (T)\n",
    "H0 = GdW30.hGdW30(D, E, H)\n",
    "V = GdW30.vGdW30(H_m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "eigenvalues, eigenstates = H0.eigenstates()\n",
    "#qt.fileio.qsave(eigenstates, \"eigenstates\")\n",
    "eigenstates = qt.fileio.qload(\"eigenstates\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# In principle, we could just transform V with the recently obtained eigenstates. Unfortunately, that\n",
    "# would make the test results different in different computers, as the eigenstates can have different phases.\n",
    "H0 = H0.transform(eigenstates) - eigenvalues[0]\n",
    "V = V.transform(eigenstates)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, [V])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = np.zeros(dim-1)\n",
    "taui = np.zeros(dim-1)\n",
    "for i in range(dim-1):\n",
    "    w[i] = eigenvalues[i+1] - eigenvalues[i]\n",
    "    taui[i] = 2.0*np.pi/w[i]\n",
    "    print(\"Transition {:d}: w = {:f} MHz, tau = {:f} ns\".format(i, w[i], 1000.0*taui[i]/(2.0*np.pi)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Time array definition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 5*taui[0]\n",
    "print(\"T = {:f} us*2*pi = {:f} ns\".format(T, 1000*T/(2.0*np.pi)))\n",
    "time = math_extra.timegrid(H0, T, 2.0)\n",
    "print('# Time steps =', time.shape[0])\n",
    "print('# Delta t =', time[1])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Control function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The control function is parametrized with the Fourier expansion as follow:\n",
    "\\begin{equation}\n",
    "    f(u, t) = \\frac{1}{\\sqrt{T}}u_0 + \\frac{2}{\\sqrt{T}}\\sum_{k = 1}^{M}u_{2k}\\cos(\\omega_kt) + \\frac{2}{\\sqrt{T}}\\sum_{k = 1}^{M}u_{2k + 1}\\sin(\\omega_kt),\n",
    "\\end{equation}\n",
    "where $u_0\\dots u_{2M + 1}$ are the control parameters. This way, we can compute the derivate respect any control parameter as\n",
    "\\begin{equation}\n",
    "    \\frac{\\partial f}{\\partial u_m}(u, t) = f(e_m, t),\n",
    "\\end{equation}\n",
    "where $e_m$ is the set of parameters where all of them are zero except the m-th ane, that is equal to one.\n",
    "\n",
    "This pulse parametrization is included in the typical_pulses.py file as pulse class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 10\n",
    "\n",
    "omega = np.zeros(M+1)\n",
    "omega[0] = 0.0\n",
    "for k in range(1, M+1):\n",
    "    omega[k] = (2.0*np.pi/T) * k\n",
    "    print(\"omega[{:d}] = {:f} MHz\".format(k, omega[k]))\n",
    "\n",
    "# u = 1.0*np.random.rand((2*M + 1))\n",
    "u = np.zeros(2*M+1)\n",
    "u[2] = 1.0\n",
    "u[3] = 1.0\n",
    "f = pulses.pulse(\"fourier\", T, u = u)\n",
    "\n",
    "print(\"Amplitudes:\")\n",
    "print(\"{:f} mT\".format(u[0]/np.sqrt(T)))\n",
    "for m in range(1, 2*M+1):\n",
    "    print(\"{:f} mT\".format(2.0*u[m]/(np.sqrt(T))))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ut = f.fu(time)\n",
    "ft = pulses.pulse(\"realtime\", T, u = ut)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize = (3, 2))\n",
    "\n",
    "ax.plot(time * 1000/(2.0*np.pi), ft.fu(time), marker = '.', linewidth = 0)\n",
    "ax.plot(time * 1000/(2.0*np.pi), f.fu(time))\n",
    "ax.set_xlabel(\"Time (ns)\")\n",
    "ax.set_xlim(left = 0.0, right = time[-1]*1000/(2.0*np.pi))\n",
    "ax.set_ylabel(\"f(t) (mT)\")\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig(\"pulse.pdf\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(figsize = (3,2))\n",
    "\n",
    "ax.plot(time * 1000/(2.0*np.pi), ft.dfu(time, 5), marker = '.', linewidth = 0)\n",
    "#ax.plot(time * 1000/(2.0*np.pi), f.dfu(time, u, 5))\n",
    "ax.set_xlabel(\"Time (ns)\")\n",
    "ax.set_xlim(left = 0.0, right = time[-1]*1000/(2.0*np.pi))\n",
    "ax.set_ylabel(\"f(t) (mT)\")\n",
    "if isnotebook:\n",
    "    plt.show()\n",
    "else:\n",
    "    fig.savefig(\"pulse.pdf\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# QOCT equations "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Given the initial state of the system we know that:\n",
    "\\begin{equation}\n",
    "    i\\frac{d}{dt}c(t) = [\\hat{H}_0 + f(u, t)\\hat{V}]c(t)\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "    c(0) = c_0\n",
    "\\end{equation}\n",
    "\n",
    "if we want to induce a transition from the initial state to another one first we need to define a function to optimize such that it minimizes when the control parameters generate the pulse that induce the transition. In this case, this function is defined as\n",
    "\\begin{equation}\n",
    "    G(u) = F(c[u], u) = J_1(c[u](T)))\n",
    "\\end{equation}\n",
    "where\n",
    "\\begin{equation}\n",
    "    J_1(c) = c^\\dagger O c\\hspace{1 cm} \\text{ with }\\hspace{1 cm} O = c_{target}c_{target}^\\dagger\n",
    "\\end{equation}\n",
    "This way, the gradient of $G(u)$ is given by\n",
    "\\begin{equation}\n",
    "    \\frac{\\partial G}{\\partial u_m}(u) = 2\\text{Im}\\int_o^Tdt\\frac{\\partial f}{\\partial u_m}(u, t)d^\\dagger[u](t)\\hat{V}c[u](t)\n",
    "\\end{equation}\n",
    "Where $d[u](t)$ is called \"costate\" and can be obtained as following:\n",
    "\\begin{equation}\n",
    "    i\\frac{d}{dt}d(t) = \\hat{H}^\\dagger(u, t)d[u](t)\n",
    "\\end{equation}\n",
    "\\begin{equation}\n",
    "    d[u](T) = Oc[u](T)\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# QOCT target function definition"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lambda_ = 0.1 / sp.integrate.simpson(f.fu(time)*f.fu(time), x = time)\n",
    "\n",
    "def Pfunction(u):\n",
    "    return - lambda_ * sp.integrate.simpson(f.fu(time) * f.fu(time), x = time)\n",
    "\n",
    "def dPdu(u, m):\n",
    "    return - 2.0 * lambda_ * sp.integrate.simpson(f.dfu(time, m) * f.fu(time), x = time)\n",
    "\n",
    "def Fyu(y, u):\n",
    "    return qt.expect(qt.fock_dm(dim, target_level), y) + Pfunction(u)\n",
    "\n",
    "def dFdy(y, u):\n",
    "    return qt.fock_dm(dim, target_level) * y\n",
    "\n",
    "def dFdu(u, m):\n",
    "    return dPdu(u, m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('generic', Fyu = Fyu, dFdy = dFdy, dFdu = dFdu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "state_0 = qt.basis(dim, 0) #initial state"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison, Schrödinger picture"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, time.shape[0], tg, f, state_0,\n",
    "                interaction_picture = False, solve_method = 'cfmagnus4')\n",
    "print(\"The value of the target function is {}\".format(opt.gfunc(u)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "data.append(derqoct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison, interaction picture"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, time.shape[0], tg, f, state_0,\n",
    "                interaction_picture = True, solve_method = 'cfmagnus4')\n",
    "print(\"The value of the target function is {}\".format(opt.gfunc(u)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "data.append(derqoct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison, Schrödinger picture, Hamiltonian-as-a-function"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Hfunc(t, args):\n",
    "    return H0 + args[\"f\"][0](t) * V\n",
    "def Vfunc(t, args):\n",
    "    return V\n",
    "H_ = hamiltonians.hamiltonian(Hfunc, Vfunc)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H_, T, time.shape[0], tg, f, state_0,\n",
    "                interaction_picture = False, solve_method = 'sesolve')\n",
    "print(\"The value of the target function is {}\".format(opt.gfunc(u)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "data.append(derqoct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison, Schrödinger picture, real-time parametrization of the pulse"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We need to redefine these functions, because they were done in terms of `f`, and not `ft`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lambda_ = 0.1 / sp.integrate.simpson(ft.fu(time)*ft.fu(time), x = time)\n",
    "\n",
    "def Pfunction(u):\n",
    "    return - lambda_ * sp.integrate.simpson(ft.fu(time) * ft.fu(time), x = time)\n",
    "\n",
    "def dPdu(u, m):\n",
    "    return - 2.0 * lambda_ * sp.integrate.simpson(ft.dfu(time, m) * ft.fu(time), x = time)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('generic', Fyu = Fyu, dFdy = dFdy, dFdu = dFdu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, time.shape[0], tg, ft, state_0,\n",
    "                interaction_picture = False, solve_method = 'cfmagnus4')\n",
    "print(\"The value of the target function is {}\".format(opt.gfunc(ut)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(ut)\n",
    "print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "data.append(derqoct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Comparison, Schrödinger picture, two perturbations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = np.array([0.15, 0, 0.0], dtype = float) #magnetic field in T\n",
    "H_m1 = np.array([0, 0.001, 0], dtype = float) #only in presence of perturbation (T)\n",
    "H_m2 = np.array([0, 0.0, 0.001], dtype = float) #only in presence of perturbation (T)\n",
    "H0 = GdW30.hGdW30(D, E, H)\n",
    "V1 = GdW30.vGdW30(H_m1)\n",
    "V2 = GdW30.vGdW30(H_m2)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# In principle, we could just transform V with the recently obtained eigenstates. Unfortunately, that\n",
    "# would make the test results different in different computers, as the eigenstates can have different phases.\n",
    "H0 = H0.transform(eigenstates) - eigenvalues[0]\n",
    "V = V.transform(eigenstates)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, [V1, V2])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 10\n",
    "\n",
    "# u = 1.0*np.random.rand((2*M + 1))\n",
    "u1 = np.zeros(2*M+1)\n",
    "u2 = np.zeros(2*M+1)\n",
    "u1[2] = 1.0\n",
    "u1[3] = 1.0\n",
    "u2[4] = 1.0\n",
    "u2[5] = 1.0\n",
    "f1 = pulses.pulse(\"fourier\", T, u = u1)\n",
    "f2 = pulses.pulse(\"fourier\", T, u = u2)\n",
    "f = [f1, f2]\n",
    "u = pulses.pulse_collection_get_parameters(f)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lambda_ = 0.1 / sp.integrate.simpson(f[0].fu(time)*f[0].fu(time), x = time) + \\\n",
    "          0.1 / sp.integrate.simpson(f[1].fu(time)*f[1].fu(time), x = time)\n",
    "\n",
    "def Pfunction(u):\n",
    "    pulses.pulse_collection_set_parameters(f, u)\n",
    "    return - lambda_ * sp.integrate.simpson(f[0].fu(time) * f[0].fu(time), x = time) \\\n",
    "           - lambda_ * sp.integrate.simpson(f[1].fu(time) * f[1].fu(time), x = time)\n",
    "\n",
    "def dPdu(u, m):\n",
    "    pulses.pulse_collection_set_parameters(f, u)\n",
    "    l = pulses.pulse_collection_l(f, m)\n",
    "    j = pulses.pulse_collection_j(f, m)\n",
    "    return - 2.0 * lambda_ * sp.integrate.simpson(f[l].dfu(time, j) * f[l].fu(time), x = time)\n",
    "\n",
    "def Fyu(y, u):\n",
    "    return qt.expect(qt.fock_dm(dim, target_level), y) #+ Pfunction(u)\n",
    "\n",
    "def dFdy(y, u):\n",
    "    return qt.fock_dm(dim, target_level) * y\n",
    "\n",
    "def dFdu(u, m):\n",
    "    return 0.0 # dPdu(u, m)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('generic', Fyu = Fyu, dFdy = dFdy, dFdu = dFdu)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, time.shape[0], tg, f, state_0,\n",
    "                interaction_picture = False, solve_method = 'sesolve')\n",
    "print(\"The value of the target function is {}\".format(opt.gfunc(ut)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "data.append(derqoct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Output data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"data\", \"w\") as datafile:\n",
    "    for i in data:\n",
    "        datafile.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "ceb332c1-c75d-4305-badc-6e4ac186f70c",
   "metadata": {},
   "source": [
    "# Rotations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7c56212a-d417-4fd1-a344-af9357335050",
   "metadata": {},
   "source": [
    "The purpose of this same notebook is to demonstrate the use of monochromatic pulses to create unitaries, when these are docomponsed into sequences of rotations between levels. We will use some functions in the pulses module that create the pulses that induce sequences of rotations. Then, we will use the solvers module to propagate the system and demonstrate how the pulses work (approximately). To clarify evertyhing, we quickly summarize here some theoretical results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c4e4fcb2-1b64-435c-a83d-f4f2a35c89e1",
   "metadata": {},
   "outputs": [],
   "source": [
    "try:\n",
    "    get_ipython\n",
    "    isnotebook = True\n",
    "except:\n",
    "    isnotebook = False\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib\n",
    "import qutip as qt\n",
    "import qutip_qip.operations as operations\n",
    "from time import time as clocktime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c12d207-d291-4703-b95b-9774157a5fa8",
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.models.GdW30 as GdW30\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.math_extra as math_extra\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.solvers as solvers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8785d7dc-0f4d-49ed-a8b5-bf2a124f8c44",
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "70f515d7-7091-4376-a89a-52af8e59df40",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15a5ef6d-19d3-4935-805f-4eae31b49556",
   "metadata": {},
   "source": [
    "# Rotation operators"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d3521556-cdbd-415f-9332-8dc3aceb3cc6",
   "metadata": {},
   "source": [
    "Any unitary evolution operator on a two-level system (TLS) can be written as a rotation operator in the form:\n",
    "\\begin{equation}\n",
    "R_{\\vec{n}}(\\theta) = e^{-i\\frac{\\theta}{2}\\vec{n}\\vec{\\sigma}}\\,,\n",
    "\\end{equation}\n",
    "times some global uninteresting phase factor.\n",
    "$\\vec{n}$ is a unit three-dimensional vector, and $\\theta \\in [0, 4\\pi)$. For more than two levels, one can speak of rotation operators within two-dimensional subspaces, $R^{(ij)}_{\\vec{n}}(\\theta)$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "39015c3a-cba7-45de-90d6-455d8f34f80e",
   "metadata": {},
   "source": [
    "# A first approach to the problem: exact rotations using two monochromatic pulses"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "7dc24565-8891-4ced-8a4b-a43f38bbe773",
   "metadata": {},
   "source": [
    "First, let us approach the problem assuming that we have a generic TLS that we can manipulate using two coupling operators, i.e. we have a Hamiltonian in the form:\n",
    "\\begin{equation}\n",
    "H(t) = H_0 + V(t) = \n",
    "\\left[\\begin{array}{cc}\n",
    "\\varepsilon_0 & 0\n",
    "\\\\\n",
    "0 & \\varepsilon_1\n",
    "\\end{array}\n",
    "\\right]\n",
    "+  a\\cos(\\Omega t + \\phi) \\sigma_x + a\\sin(\\Omega t + \\phi) \\sigma_y\\,.\n",
    "\\end{equation}\n",
    "We work in resonance, meaning $\\varepsilon_0-\\varepsilon_1 = \\Omega$. Note that this value can be negative. The two coupling operators are _oriented_ in the $\\sigma_x$ and $\\sigma_y$ _directions_ (this will be generalized later on). Also, note that the phase $\\phi$ is the same for both perturbations, as it is their amplitude $a$. We can then rewrite the Hamiltonian given above as:\n",
    "\\begin{equation}\n",
    "H(t) =\n",
    "\\left[\\begin{array}{cc}\n",
    "\\varepsilon_0 & 0\n",
    "\\\\\n",
    "0 & \\varepsilon_1\n",
    "\\end{array}\n",
    "\\right]\n",
    "+ a\n",
    "\\left[\\begin{array}{cc}\n",
    "0 & e^{-i(\\Omega t+\\phi)}\n",
    "\\\\\n",
    "e^{i(\\Omega t+\\phi)} & 0\n",
    "\\end{array}\n",
    "\\right]\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8be9c073-104c-4a6e-b4a9-5c51f0274e34",
   "metadata": {},
   "source": [
    "Let us now solve the problem in the interaction picture, through the transformation:\n",
    "\\begin{equation}\n",
    "T = e^{iH_0 t}\n",
    "\\end{equation}\n",
    "This transformation has the virtue of leading to a time-indepenent Schrödinger equation:\n",
    "\\begin{equation}\n",
    "\\dot{\\psi}(t) = -i a (\\cos\\phi\\sigma_x + \\sin\\phi\\sigma_y)\\psi(t)\n",
    "= -i a \\vec{n}\\vec{\\sigma}\\psi(t)\\,,\n",
    "\\end{equation}\n",
    "where $\\vec{n} = (\\cos\\phi, \\sin\\phi, 0)$ is a unit vector. The solution is:\n",
    "\\begin{equation}\n",
    "\\psi(t) = e^{-iat\\vec{n}\\vec{\\sigma}}\\psi(0)\\,.\n",
    "\\end{equation}\n",
    "One can see how in this way we have implemented an exact rotation, whose amplitude is given by $\\theta/2 = at$. Amplitude and operation time $a$ and $t$ are thus inversely related: one can implement very fast rotations using very large amplitudes; or put it differently, if one can only use small amplitudes, the operation time must be long."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1c225286-9a60-4665-9b0f-00d462156bc0",
   "metadata": {},
   "source": [
    "Therefore, using an external field with the form given above, we can implement any rotation as long as its axis\n",
    "does not contain any $z$ component. If it does, one can implement it making use of a concatenation of pulses, for example:\n",
    "\\begin{equation}\n",
    "R_Z(\\theta) = R_X(-\\pi/2)R_Y(\\theta)R_X(\\pi/2)\\,.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3da7789c-32c9-4b09-8459-13f37e909f6d",
   "metadata": {},
   "source": [
    "However, let us now suppose that the perturbations that we can use are not $\\sigma_x$ and $\\sigma_y$, but some combinations of those:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "519714f4-db2a-49df-aa2f-180b522bb3a7",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "H(t) = \n",
    "\\left[\\begin{array}{cc}\n",
    "\\varepsilon_0 & 0\n",
    "\\\\\n",
    "0 & \\varepsilon_1\n",
    "\\end{array}\n",
    "\\right]\n",
    "+ f_1(t) V_1 + f_2(t)V_2\n",
    "\\end{equation}\n",
    "where:\n",
    "\\begin{align}\n",
    "V_1 &= \\left[\n",
    "\\begin{array}{cc}0 & \\mu_1 \\\\ \\mu_1^* & 0\\end{array}\n",
    "\\right]\\,,\n",
    "\\\\\n",
    "V_2 &= \\left[\n",
    "\\begin{array}{cc}0 & \\mu_2 \\\\ \\mu_2^* & 0\\end{array}\n",
    "\\right]\\,.\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e8bab0ba-00d3-4a33-911b-7ff6b01aae93",
   "metadata": {},
   "source": [
    "Note that this case includes the previous one if we set $\\mu_1 = 1$, $\\mu_2 = -i$, and $f_1(t) = a\\cos(\\Omega t+\\phi), f_2(t) = a\\sin(\\Omega t + \\phi)$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "381acce4-b04a-4e14-a8a4-497e56dc57c8",
   "metadata": {},
   "source": [
    "One can then prove that, given once again a generic rotation in the form:\n",
    "\\begin{equation}\n",
    "R_{\\vec{n}}(\\theta) = e^{-i\\frac{\\theta}{2}\\vec{n}\\vec{\\sigma}}\\,,\n",
    "\\end{equation}\n",
    "for $\\vec{n}=(\\cos(\\phi), \\sin(\\phi))$, one can implement it with the previous Hamiltonian, by setting:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ab4769a4-d7ec-4d08-beb9-6d1eb6f0311d",
   "metadata": {},
   "source": [
    "\\begin{equation}\n",
    "\\left[\\begin{array}{c}f_1(t)\\\\f_2(t)\\end{array}\\right] = \n",
    "\\boldsymbol{\\mu}^{-1}a\n",
    "\\left[\\begin{array}{c}\\cos(\\Omega t+\\phi)\\\\\\sin(\\Omega t+\\phi)\\end{array}\\right]\n",
    "\\end{equation}"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "2af2fce3-eb3a-423b-bca3-36ca591f077c",
   "metadata": {},
   "source": [
    "where the $\\boldsymbol{\\mu}$ matrix is:\n",
    "\\begin{equation}\n",
    "\\boldsymbol{\\mu} = \\left[\\begin{array}{cc} {\\rm Re}\\mu_1 & {\\rm Re}\\mu_2 \\\\ -{\\rm Im}\\mu_1 & - {\\rm Im}{\\mu_2} \\end{array}\\right]\n",
    "\\end{equation}\n",
    "and the total propagation time $T$ must be related to $a$ by:\n",
    "\\begin{equation}\n",
    "a T = \\frac{\\theta}{2}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "acd0e20b-259c-408e-a973-4814ba46949a",
   "metadata": {},
   "source": [
    "The problem can be generalized to $N$-dimensional quantum systems and rotations within 2-dimensional subspaces. However, the induced rotations by those pulses will not be exact, due to _leakage_: the levels outside the 2-dimensional subspace that we are addressing interfere, and get populated, too. This leakage effect can be reduced if the amplitude is reduced (in fact, if the frequencies of the unwanted transitions are different from the one of the subspace, one can probably prove that the solution becomes exact in the limit of zero amplitude)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87a494ff-4ec4-4260-9657-d1cdf530b42b",
   "metadata": {},
   "source": [
    "Finally, it should be stressed that, in this way, we implement a rotation _in the interaction representation_."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d787c71-60c2-4f3b-abf3-28193b96c3a1",
   "metadata": {},
   "source": [
    "# A second approach to the problem: approximate rotations using one monochromatic pulses and the rotating wave approximation"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3ccd7467-f53f-42ba-beaa-41b006f58534",
   "metadata": {},
   "source": [
    "The previous scheme permits to obtain exact rotations for any required duration $T$, even short ones (the shorter the duration, the higher the required amplitude). However, it requires using two independent perturbation operators $V_1$ and $V_2$. What happens if can only apply one? For that case, there is an alternative albeit approximate approach to get these rotations, using the so-called rotating wave approximation (RWA). "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d905da2b-10cd-47bb-87c0-b081e6832802",
   "metadata": {},
   "source": [
    "We start by assuming a Hamiltonian in the form:\n",
    "\\begin{equation}\n",
    "H(t) = \n",
    "\\left[\n",
    "\\begin{array}{cc}\\varepsilon_0 & 0 \\\\ 0 & \\varepsilon_1 \\end{array}\n",
    "\\right]\n",
    "+ a\\cos(\\omega t + \\phi) V =\n",
    "\\left[\n",
    "\\begin{array}{cc}\\varepsilon_0 & 0 \\\\ 0 & \\varepsilon_1 \\end{array}\n",
    "\\right]\n",
    "+ a \\cos(\\omega t + \\phi)\n",
    "\\left[\\begin{array}{cc}\n",
    "0 & \\mu_0\n",
    "\\\\\n",
    "\\mu_0^* & 0\n",
    "\\end{array}\n",
    "\\right]\\,\n",
    "\\end{equation}\n",
    "where, as before, $\\varepsilon_0-\\varepsilon_1=\\Omega$. Let us not make for the moment the resonance condition, and allow for $\\omega \\ne \\Omega$."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ced3d608-1d69-4e44-b23e-46eda9e595aa",
   "metadata": {},
   "source": [
    "In the interaction picture, the Hamiltonian reads:\n",
    "\\begin{equation}\n",
    "H_I(t) = a\\cos(\\omega t + \\phi)\n",
    "\\left[\\begin{array}{cc}\n",
    "0 & \\mu_0 e^{i\\Omega t}\n",
    "\\\\\n",
    "\\mu_0^* e^{-i\\Omega t} & 0\n",
    "\\end{array}\n",
    "\\right]\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9621e28b-b3a6-497b-9b00-f35d35681f42",
   "metadata": {},
   "source": [
    "The RWA consists in expanding $\\cos(\\omega t + \\phi)$ as $\\frac{1}{2}(e^{i(\\omega t + \\phi)} + e^{-i(\\omega t t+ \\phi)})$, and neglecting all terms containing $e^{i(\\omega + \\Omega)t}$. The resulting Hamiltonian is:\n",
    "\\begin{equation}\n",
    "H_I(t) = \\frac{1}{2}a\\left[\\begin{array}{cc}\n",
    "0 & \\vert\\mu_0\\vert e^{-i(\\alpha + \\delta t)}\n",
    "\\\\\n",
    "\\vert\\mu_0\\vert e^{i(\\alpha + \\delta t)} & 0\n",
    "\\end{array}\\right]\n",
    "\\end{equation}\n",
    "where\n",
    "\\begin{align}\n",
    "\\alpha &= \\arg \\mu_0 + \\phi\n",
    "\\\\\n",
    "\\alpha &= \\phi - \\arg \\mu_0\n",
    "\\\\\n",
    "\\delta &= \\omega - \\Omega\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4ba9a6ac-a297-4159-bb59-b7fa2c1c1643",
   "metadata": {},
   "source": [
    "Note that $\\arg \\mu_0$ is the parameter that _tunes_ the perturbation between $\\sigma_x$ and $\\sigma_y$:\n",
    "\\begin{equation}\n",
    "V = \\vert \\mu_0 \\vert \\cos\\arg\\mu_0 \\sigma_x - \\vert \\mu_0 \\vert \\sin\\arg\\mu_0\\sigma_y\\,.\n",
    "\\end{equation}\n",
    "And note that the Hamiltonian $H_I$ only depends on the sum $\\alpha = \\arg\\mu_0 + \\phi$, and therefore it is not too important whether $V$ has $\\sigma_x$ or $\\sigma_y$ character, as one can always change $\\phi$ accordingly."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ded4816d-56a3-4b6c-9cd0-ccc3e1ac28c5",
   "metadata": {},
   "source": [
    "Let us know make a new unitary transformation, using:\n",
    "\\begin{equation}\n",
    "T_d(t) = \\left[\\begin{array}{cc}\n",
    "e^{-i\\frac{\\delta}{2}t} & 0 \n",
    "\\\\\n",
    "0 & e^{i\\frac{\\delta}{2}t}\n",
    "\\end{array}\\right]\n",
    "\\end{equation}\n",
    "that leads to the _final_ Hamiltonian:\n",
    "\\begin{equation}\n",
    "H'_I = \\vec{m}\\vec{\\sigma}\n",
    "\\end{equation}\n",
    "where\n",
    "\\begin{align}\n",
    "m_1 &= \\frac{1}{2}a\\vert\\mu_0\\vert\\cos\\alpha\\,,\n",
    "\\\\\n",
    "m_2 &= \\frac{1}{2}a\\vert\\mu_0\\vert\\sin\\alpha\\,,\n",
    "\\\\\n",
    "m_3 &= \\frac{1}{2}\\delta\\,.\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fb8b3982-84e6-4c81-a895-0af0eae3aa14",
   "metadata": {},
   "source": [
    "Given that this Hamiltonian is time independent, the solution is trivial:\n",
    "\\begin{equation}\n",
    "\\psi(t) = \\exp{(-i\\vec{m}\\vec{\\sigma}t)}\\psi(0)\\,.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a8244698-86bc-4e81-bea2-c51be562012c",
   "metadata": {},
   "source": [
    "We have, once again, a rotation operator. However, this is not a rotation operator in the interaction representation, which is the one that normally one is after, since we had to do the previous transformation that depends on the _dephasing_ $\\delta$. If the detuning is zero (i.e., once again, we assume the resonance condition), we are back to a rotation $R_{\\vec{n}}(\\theta)$ in the interaction representation, where:\n",
    "\\begin{align}\n",
    "\\theta &= a \\vert \\mu_0 \\vert t\n",
    "\\\\\n",
    "n_1 &= \\cos\\alpha\n",
    "\\\\\n",
    "n_2 &= \\sin\\alpha\n",
    "\\\\\n",
    "n_3 &= 0\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "23f2c4e1-b5ab-4877-b310-e165fcb445f2",
   "metadata": {},
   "source": [
    "Therefore, if we wish to implement a $R_{\\vec{n}}(\\theta)$ rotation _in the RWA_, we must (1) set the phase $\\phi$ in such a way as to make $\\cos\\alpha, \\sin\\alpha = n_1, n_2$ (it is assumed that $n_3 = 0$), and set set the total operation time $T$ and the amplitude $a$ in such a way that they fulfill:\n",
    "\\begin{equation}\n",
    "\\theta = a \\vert \\mu_0 \\vert T\\,.\n",
    "\\end{equation}\n",
    "A typical case is the need to implement the $U_X = \\sigma_x$ gate, that flips the states of a qubit:\n",
    "\\begin{equation}\n",
    "U_X = \\left[\\begin{array}{cc}0 & 1 \\\\ 1 & 0 \\end{array}\\right]\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c3f0b204-da56-4762-9c6c-0a3b70c09820",
   "metadata": {},
   "source": [
    "This unitary is however _not_ a rotation, since its determinant is not one; it is however physically equivalent to:\n",
    "\\begin{equation}\n",
    "-i U_X = R_{(1, 0, 0)}(\\pi)\n",
    "\\end{equation}\n",
    "In order to implement this gate, one must set $\\alpha = \\phi + \\arg\\mu_0 = 0$ (so that $\\cos\\alpha = 1$, and set $a$ and $T$ so that $\\pi = \\vert \\mu_0\\vert aT$ (this is called a $\\pi$-pulse)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6dd78c70-9213-4199-9713-a600ef227a8a",
   "metadata": {},
   "source": [
    "# Rotation sequences"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "84d57b19-5c7a-49cd-836e-b9e379f0a148",
   "metadata": {},
   "source": [
    "One can prove that, except for a global phase factor, any unitary in a $N$-level system can be decomposed into a sequence of rotations:\n",
    "\\begin{equation}\n",
    "U = R_{\\vec{n}_K}(\\theta_K)R_{\\vec{n}_{K-1}}(\\theta_{K-1})\\dots R_{\\vec{n}_1}(\\theta_1)\\,.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c29f3211-ebc5-4db0-ab0b-78af09f4c062",
   "metadata": {},
   "source": [
    "For example, in an 8-level system, the Toffoli gate is given by:\n",
    "\\begin{align}\n",
    "    U_{\\rm Toffoli} = e^{i\\frac{\\pi}{8}} &\n",
    "    R_{Z}^{(01)}(\\frac{1}{4}\\pi)\n",
    "    R_{Z}^{(12)}(\\frac{1}{2}\\pi)\n",
    "    R_{Z}^{(23)}(\\frac{3}{4}\\pi)\n",
    "    R_{Z}^{(34)}(\\pi)\n",
    "    \\\\\n",
    "    &\n",
    "    R_{Z}^{(45)}(\\frac{5}{4}\\pi)\n",
    "    R_{Z}^{(56)}(\\frac{3}{2}\\pi)\n",
    "    R_{Z}^{(67)}(\\frac{3}{4}\\pi)\n",
    "    R_{X}^{(67)}(\\pi)\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "25d6886f-2e85-4f4f-914c-2a4e94584cde",
   "metadata": {},
   "source": [
    "# Model"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "id": "2d48ec30-1928-43f5-bb9b-d0aaa8d75d50",
   "metadata": {},
   "source": [
    "The system that we consider here is the GdW$_{30}$ complex [M. D. Jenkins et al., Physical Review B 95, 1 (2017)]. Its core is a Gd$^{3+}$ ion with a $4f^7$ configuration, whose ground manifold has $L = 0$ and $S = 7/2$. This $d$-level manifold ($d = 2S + 1 = 8$) can be considered a qudit. Under the effect of a DC magnetic field $\\vec{H}$, the spin Hamiltonian of this molecule can be well approximated by an orthorhombic zero field splitting plus a Zeeman contribution:\n",
    "\\begin{equation}\n",
    "H_0 = D\\bigg[S_z^2 - \\frac{1}{3}S(S + 1)\\bigg] + E[S_x^2 - S_y^2] - g\\mu_B\\vec{S}\\cdot\\vec{H}\n",
    "\\end{equation}\n",
    "In our case, $S = 7/2$, $D$ = 1281 MHz, $E$ = 294 MHz, $\\vec{H} = (0.15, 0.0, 0.0)$ T."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91fe35ac-c4ff-43c7-85b9-0504422f2154",
   "metadata": {},
   "source": [
    "To this equation, we can add a time-dependent perturbation:\n",
    "\\begin{equation}\n",
    "H(t) = H_0 + f_1(t)V_1 + f_2(t)V_2\\,.\n",
    "\\end{equation}\n",
    "The perturbations are magnetic field:\n",
    "\\begin{equation}\n",
    "V_i = -g\\mu_B\\vec{S}\\cdot\\vec{H}_i \\,.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e22560cb-5952-4857-ba76-ed1631c449a3",
   "metadata": {},
   "outputs": [],
   "source": [
    "S = 7/2 # spin\n",
    "E = 294 # value in MHz\n",
    "D = 1281 # value in MHz\n",
    "dim = int(2*S + 1) #matrix dim\n",
    "n = dim-1 # max level of the final state\n",
    "H = np.array([0.15, 0, 0.0], dtype = float) #magnetic field in T\n",
    "H_m1 = np.array([0, 0.001, 0], dtype = float) #only in presence of perturbation (T)\n",
    "H_m2 = np.array([0, 0, 0.001], dtype = float)\n",
    "H0 = GdW30.hGdW30(D, E, H)\n",
    "V1 = GdW30.vGdW30(H_m1)\n",
    "V2 = GdW30.vGdW30(H_m2)\n",
    "eigenvalues, eigenstates = H0.eigenstates()\n",
    "H0 = H0.transform(eigenstates)\n",
    "V1 = V1.transform(eigenstates)\n",
    "V2 = V2.transform(eigenstates)\n",
    "# The transformation may have \"disordered\" the levels. We have a diagonal matrix, and therefore\n",
    "# the eigenvalues are the diagonal elements. The eigenvectors are the unit vectors in each direction.\n",
    "eigenvalues = H0.diag()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d1287c8-627a-4f9b-bb79-b76a520bbd39",
   "metadata": {},
   "source": [
    "Note that we are using, as perturbation operators, $\\vec{H}_1$ in the $y$ direction, and $\\vec{H}_2$ in the $z$ direction. In this way they are both linear combinations of $\\sigma_x$ and $\\sigma_y$ (within each 2-dimensional subspace). Now we compute all the transition frequencies (between nearest neighbours in energy). Since the most relevant\n",
    "frequency for a Toffoli gate is the one between the sixth and the seventh levels, we will use that as the \"reference\" energy, $\\Omega = \\varepsilon_6 - \\varepsilon_7$, and the reference period $\\tau = \\frac{2\\pi}{\\Omega}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "df8a3c12-49f8-435f-8d69-c264a7d2246f",
   "metadata": {},
   "outputs": [],
   "source": [
    "w = np.zeros(dim-1)\n",
    "tau = np.zeros(dim-1)\n",
    "for i in range(dim - 1):\n",
    "    w[i] = eigenvalues[i] - eigenvalues[i+1] #in MHz\n",
    "    tau[i] = 2.0*np.pi/w[i]\n",
    "\n",
    "Omega = w[dim-2]\n",
    "Tau = 2.0 * np.pi / np.abs(Omega)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3c36208d-d5ab-4b51-a0b7-500b4d5f0585",
   "metadata": {},
   "source": [
    "Now we define the sequence of rotations. Each rotation is a list ``[axis, i, j, \\theta]``, where ``axis`` is 0, 1, 2 depending on whether we want a $X$, $Y$, or $Z$ rotation, $i$ and $j$ are the levels addressed by the rotation, and $\\theta$ is the angle. A sequence of rotations is obviously a list of rotations. We will use, for this example, the Toffoli sequence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "0c9fcc57-49f6-43ca-a850-e9f8dcd1142c",
   "metadata": {},
   "outputs": [],
   "source": [
    "xaxis = 0\n",
    "yaxis = 1\n",
    "zaxis = 2\n",
    "\n",
    "toffoli_sequence = [ [xaxis, 6, 7, np.pi],\n",
    "                     [zaxis, 6, 7, (3/4)*np.pi],\n",
    "                     [zaxis, 5, 6, (3/2)*np.pi],\n",
    "                     [zaxis, 4, 5, (5/4)*np.pi],\n",
    "                     [zaxis, 3, 4, (1)*np.pi],\n",
    "                     [zaxis, 2, 3, (3/4)*np.pi],\n",
    "                     [zaxis, 1, 2, (1/2)*np.pi],\n",
    "                     [zaxis, 0, 1, (1/4)*np.pi] ]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb31ba21-bb51-40fa-8866-9328e722ccc2",
   "metadata": {},
   "source": [
    "Instead of using the full Toffoli gate, we consider only the first two rotations (that, in fact, are four), to make things faster:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4ac8773a-b0d9-4630-941e-8f37097f058b",
   "metadata": {},
   "outputs": [],
   "source": [
    "target_sequence = toffoli_sequence[0:2]\n",
    "\n",
    "Utarget = pulses.pulse_sequence_U(target_sequence, dim)\n",
    "UToffoli = qt.Qobj(operations.toffoli().full())"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40148d0c-0ad9-417a-bdb2-68c5a4107727",
   "metadata": {},
   "source": [
    "# Time-propagations"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "21519466-d9a9-4184-8bb6-3adbb6b87d1d",
   "metadata": {},
   "source": [
    "We will now create the pulses objects that implement the sequence given above. We will fix the maximum amplitude; for the two-perturbation case, note how the two perturbations may have different amplitudes, and therefore we must speak of a maximum amplitude between the two. For the single perturbation case, the maximum amplitude is just the one amplitude of all the pulses of the sequence."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "c42d0c84-2c9c-4226-8e2a-0697d293abbf",
   "metadata": {},
   "outputs": [],
   "source": [
    "amplitude = 1.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fecae73c-546e-433d-ac5c-e5306a0b0f85",
   "metadata": {},
   "outputs": [],
   "source": [
    "amp = 1.0 \n",
    "f, maxcoeff = pulses.pulse_sequence2(target_sequence, amp, 0.0, V1, V2, eigenvalues)\n",
    "f, maxcoeff = pulses.pulse_sequence2(target_sequence, (amplitude / maxcoeff) * amp, 0.0, V1, V2, eigenvalues)\n",
    "\n",
    "T = f[0].T\n",
    "ncycles = T / Tau\n",
    "print(\"ncycles = {}\".format(ncycles))\n",
    "\n",
    "ntstepspercycle = 50\n",
    "ntsteps = round(ntstepspercycle * ncycles)\n",
    "ts = np.linspace(0, T, ntsteps)\n",
    "\n",
    "t0 = clocktime()\n",
    "result = solvers.solve('cfmagnus4', hamiltonians.hamiltonian(H0, [V1, V2]),\n",
    "                       f, qt.identity(dim), ts,\n",
    "                       interaction_picture = True,\n",
    "                       returnQoutput = False)\n",
    "t1 = clocktime()\n",
    "print(\"Elapsed time = {}\".format(t1-t0))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2c464148-53de-4694-8a69-3919f5fd36af",
   "metadata": {},
   "outputs": [],
   "source": [
    "fsinglecombined = pulses.pulse_sequence(target_sequence, amplitude, 0.0, V1, eigenvalues)\n",
    "\n",
    "T = fsinglecombined.T\n",
    "ncycles = T / Tau\n",
    "print(\"ncycles (single) = {}\".format(ncycles))\n",
    "\n",
    "ntstepspercycle = 50\n",
    "ntsteps = round(ntstepspercycle * ncycles)\n",
    "ts = np.linspace(0, T, ntsteps)\n",
    "\n",
    "t0 = clocktime()\n",
    "result_single = solvers.solve('cfmagnus4', hamiltonians.hamiltonian(H0, [V1]),\n",
    "                              fsinglecombined, qt.identity(dim), ts,\n",
    "                              interaction_picture = True,\n",
    "                              returnQoutput = False)\n",
    "t1 = clocktime()\n",
    "print(\"Elapsed time = {}\".format(t1-t0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "833173c5-545b-40c9-86f0-5836c6819be1",
   "metadata": {},
   "source": [
    "As a measure of the fidelity of the induced unitary, we will use:\n",
    "\\begin{equation}\n",
    "F(U) = \\frac{1}{\\rm dim}\\vert {\\rm Tr}\\left[U^\\dagger U_{\\rm target}\\right]\\vert\\,.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "31b6cf8b-730b-4d69-a584-ebcfeec2d688",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Had we done the calculations in the Schrödinger representation, we would\n",
    "# need to do a transformation here:\n",
    "#res = (1j * H0 * ts[-1]).expm() * qt.Qobj(result[-1])\n",
    "\n",
    "res = qt.Qobj(result[-1])\n",
    "fu = np.abs(Utarget.overlap(res)/dim)\n",
    "print(\"F(U) (double pulse) = {}\".format(fu))\n",
    "data.append(fu)\n",
    "\n",
    "res = qt.Qobj(result_single[-1])\n",
    "fu = np.abs(Utarget.overlap(res)/dim)\n",
    "print(\"F(U) (single pulse) = {}\".format(fu))\n",
    "data.append(fu)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "80d842da-b14e-4e8f-adcc-f1f0e4844e19",
   "metadata": {},
   "source": [
    "Note how neither of the two propagations have produced an exact result. In the two-perturbations case, the error is due to leakage; in the one-perturbation case, the error is due to both leakage and the rotating wave approximation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f78c8c23-c112-40d5-8559-d9b0ebdaf03f",
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(\"data\", \"w\") as f:\n",
    "    for i in data:\n",
    "        f.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.11.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

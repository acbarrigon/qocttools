## Copyright 2019-present The qocttools developing team
##
## This file is part of qocttools.
##
## qocttools is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## qocttools is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with qocttools.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
import os
import tempfile
import shutil
import numpy as np
from distutils.dir_util import copy_tree
import pytest


def run_test(dire, fname, datafile = None, reltol = 1.0e-6, abstol = 1.0e-12):
    tempdir = tempfile.mkdtemp()
    cdir = os.getcwd()
    dirname = os.path.dirname(__file__) + dire
    copy_tree(dirname, tempdir)
    os.chdir(tempdir)
    i = subprocess.call('jupyter nbconvert ' + fname + ' --execute --to python', shell = True)

    datacomp = True
    if datafile is not None:
        data = np.loadtxt(datafile)
        dataref = np.loadtxt(datafile) #+'.ref')
        datacomp = (data == pytest.approx(dataref, rel = reltol, abs = abstol))

    os.chdir(cdir)
    if i == 0 and datacomp:
        shutil.rmtree(tempdir)
    else:
        print("Failed test directory at " + tempdir)

    return i, datacomp


def test_rotations():
    i, datacomp = run_test('/../sampleruns/rotations', 'rotations.ipynb', datafile = 'data.ref')
    assert (i == 0) and datacomp

def test_propagators_psi():
    i, datacomp = run_test('/../sampleruns/propagators-psi', 'propagators-psi.ipynb', datafile = 'data.ref',
                              reltol = 1.0e-3, abstol = 1.0e-8)
    assert (i == 0) and datacomp

def test_propagators_U():
    i, datacomp = run_test('/../sampleruns/propagators-U', 'propagators-U.ipynb', datafile = 'data.ref',
                              reltol = 1.0e-3, abstol = 1.0e-8)
    assert  (i == 0) and datacomp

def test_propagators_rho():
    i, datacomp = run_test('/../sampleruns/propagators-rho', 'propagators-rho.ipynb', datafile = 'data.ref',
                              reltol = 1.0e-3, abstol = 1.0e-8)
    assert  (i == 0) and datacomp

def test_gradient_psi():
    i, datacomp = run_test('/../sampleruns/gradient-psi', 'gradient-psi.ipynb', datafile = 'data.ref')
    assert  (i == 0) and datacomp

def test_gradient_U():
    i, datacomp = run_test('/../sampleruns/gradient-U', 'gradient-U.ipynb', datafile = 'data.ref')
    assert  (i == 0) and datacomp

def test_gradient_rho():
    i, datacomp = run_test('/../sampleruns/gradient-rho', 'gradient-rho.ipynb', datafile = 'data.ref')
    assert  (i == 0) and datacomp

def test_qoctbasic():
    i, datacomp = run_test('/../sampleruns/qoctbasic', 'qoctbasic.ipynb', datafile = 'data.ref')
    assert  (i == 0) and datacomp

def test_floquet():
    i, datacomp = run_test('/../sampleruns/floquet', 'floquet', datafile = 'data.ref')
    assert  (i == 0) and datacomp

def test_tutorials_closed():
    i, datacomp = run_test('/../../docs/tutorials/', "closed.ipynb", datafile = 'data.closed')
    assert i == 0 and datacomp

def test_tutorials_closed_gate():
    i, datacomp = run_test('/../../docs/tutorials/', "closed-gate.ipynb", datafile = 'data.closed_gate')
    assert i == 0 and datacomp

def test_tutorials_closed_gate_cnot():
    i, datacomp = run_test('/../../docs/tutorials/', "closed-gate-cnot.ipynb", datafile = 'data.closed_gate_cnot')
    assert i == 0 and datacomp

def test_tutorials_floquet_closed():
    i, datacomp = run_test('/../../docs/tutorials/', "floquet-closed.ipynb", datafile = 'data.floquet_closed')
    assert i == 0 and datacomp

def test_tutorials_floquet_open():
    i, datacomp = run_test('/../../docs/tutorials/', "floquet-open.ipynb", datafile = 'data.floquet_open')
    assert i == 0 and datacomp

# This file should be run as "pytest -v short.py"
# Or, even better: "pytest -n NPROCS --durations=0 -v short.py"
# where NPROCS is the number of cores that can be used in parallel.

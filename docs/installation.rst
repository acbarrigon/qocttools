.. _Installation:

============
Installation
============


For users
---------

You may download the code from the 
`Python Package Index PyPi <https://pypi.org/>`_. 

.. code-block:: bash

   pip install qocttools

For developers
--------------

Clone it first from the
`gitlab repository <https://gitlab.com/acbarrigon/qocttools>`_:

.. code-block:: bash

   git clone git@gitlab.com:acbarrigon/qocttools.git qocttools

You may then install it in development mode by doing

.. code-block:: bash

   pip install -e .

Probably you want to do that in a virtual environment.

You may then check that everything is fine by doing (once that
you have activated the virtual environment):

.. code-block:: bash

   pytest -v tests/short.py

.. _Articles:

===========================================
Articles created with the help of qocttools
===========================================

(or preliminary versions of the code)


1. :cite:p:`Castro2023`: Floquet engineering non-equilibrium steady states.

2. :cite:p:`PhysRevResearch.4.033213`: Floquet engineering the band structure of materials with optimal control theory

3. :cite:p:`PhysRevApplied.17.064028`: Optimal Control of Molecular Spin Qudits

.. _Tutorials:

=========
Tutorials
=========

.. We may have a toctree with hidden entries,
   and add explictly the links and a list with them, or just have
   a toctree without the "hidden" keyword.

.. _closed: tutorials/closed.ipynb
.. _closed-gate: tutorials/closed-gate.ipynb
.. _closed-gate-cnot: tutorials/closed-gate-cnot.ipynb
.. _open: tutorials/open.ipynb
.. _floquet-closed: tutorials/floquet-closed.ipynb
.. _floquet-open: tutorials/floquet-open.ipynb


1. closed_

   This tutorial demonstrates a very simple QOCT calculation for a closed
   quantum system. The target is a transition: driving the system from
   the ground to the excited state.

2. closed-gate_

   This tutorial also does one optimization for a closed system, but in this
   case the goal is to create a target evolution operator (a *gate*, in the
   languate of quantum information theory).

3. closed-gate-cnot_

   This tutorial also does one optimization for a closed system and with
   the goal of creatin a target evolution operator (a *gate*, in the
   languate of quantum information theory). In this case, the sytem is
   a two-qubit system, and the target is the CNOT gate. Furthermore, the
   problem is also solved using the QuTiP GRAPE implementation.

4. open_

   Optimization for an open system. The target is also the transfer of
   population between the ground and the first excited state of a system,
   but in the presence of dissipative terms.

5. floquet-closed_

   Floquet optimization for a closed system.

6. floquet-open_

   Floquet optimization for an open system (optimization of a NESS).



.. toctree::
   :hidden:

   tutorials/closed
   tutorials/closed-gate
   tutorials/closed-gate-cnot
   tutorials/open
   tutorials/floquet-closed
   tutorials/floquet-open

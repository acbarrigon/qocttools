.. _Method:

======
Method
======

In this page we discuss how the code attempts to solve the problems
that were described in the :ref:`Introduction`.

Recall that the task is, in its essence, to find the maximum of a function
:math:`G(u)` of a set of parameters :math:`u_1, \dots, u_M`.
This task may be accomplished with one of the thousands of optimization algorithms
that have been invented. Many of them require the gradient :math:`\nabla G(u)`.

So, we will present in :numref:`gradient_ch`, :numref:`gradient_cu`,
:numref:`gradient_od`, :numref:`gradient_floquetc`, and :numref:`gradient_floqueto`
the equations that are used to compute that gradient.

Then, we will briefly discuss the possible parametrizations, and how the
optimization is done (essentially, by externalizing the job to the
`nlopt <https://nlopt.readthedocs.io/en/latest/>`_ library).

.. _gradient_ch:

Closed systems, Hilbert state propagations
==========================================

We consider a situation in which a quantum system is closed,
and therefore we consider a situation modeled by:

.. math:: 
   i\frac{\partial}{\partial t}\vert\phi(t)\rangle = H(u, t)\vert\phi(t)\rangle
   :label: schrodinger2
.. math::
   \vert\phi(0)\rangle = \vert\phi_0\rangle
   :label: schinitstate

and we wish to find the optimal control parameters :math:`u` that maximize
a functional of the system trajectory:

.. math::
   F = F(\phi(T), u)
   :label: targetfunctionphi

that is, the maximization of function

.. math::
   G(u) = F(\phi_u(T), u)
   :label: targetfunctionGphi

The key problem that qocttools is designed to solve is the computation
of the gradient :math:`\nabla G(u)` and, using this gradient, finding
the maximal value of :math:`G` using one optimization algorithm. The
expression for the gradient that qocttools uses is:

.. math::
   \frac{\partial G}{\partial u_m}(u) =
   \frac{\partial F}{\partial u}(\psi_u(T), u) + 
   2 {\rm Im} \int_0^T\!\!{\rm d}t\; \langle \chi_u(t) \vert
   \frac{\partial H}{\partial u_m}(u) \vert \psi_u(t)\rangle
   :label: gradientpsi

where the *costate* :math:`\vert\chi_u(t)\rangle` is given by:

.. math:: 
   i\frac{\partial}{\partial t}\vert\chi_u(t)\rangle = H^{\dagger}(u, t)\vert\chi_u(t)\rangle
   :label: costatepsi
.. math::
   \vert\chi_u(T)\rangle = \frac{\partial F}{\partial \psi^*_u(T)}(\psi_u(T), u)
   :label: costatepsiinitstate

One may define arbitrary expressions for :math:`F`; however, some typical
choices are the most common. See the documentation of the :class:`target.Target` class
for the various *predefined* options. For example, a very common target is would
be the expectation value of some operator :math:`O`:

.. math::
   F(\psi(T), u) = \langle\psi(T)\vert O \vert\psi(T)\rangle
   :label: targeto

In this case, Eq. :math:numref:`costatepsiinitstate` takes the simple form:

.. math::
   \vert\chi_u(T)\rangle = O \vert\psi_u(T)\rangle
   :label: costatepsiinitstateo

In any case, the user may define its own function for function :math:`F` and pass
it to qocttools, in case the options already defined are not suitable.

The code also admits the option of defining several initial states, and a target
functional defined in terms of them. The model is in this case a set of equations:

.. math:: 
   i\frac{\partial}{\partial t}\vert\phi^i(t)\rangle = H^i(u, t)\vert\phi^i(t)\rangle
   :label: schrodingers2
.. math::
   \vert\phi^i(0)\rangle = \vert\phi^i_0\rangle
   :label: schinitstates

The target definition would then involve all systems:

.. math::
   F = F(\lbrace\phi^i(T)\rbrace_i, u)
   :label: targetfunctionphis

and the gradient expression would be a simple generalization of the previous gradient expression:

.. math::
   \frac{\partial G}{\partial u_m}(u) =
   \frac{\partial F}{\partial u}(\lbrace\psi^i_u(T)\rbrace_i, u) + \sum_i
   2 {\rm Im} \int_0^T\!\!{\rm d}t\; \langle \chi^i_u(t) \vert
   \frac{\partial H}{\partial u_m}(u) \vert \psi^i_u(t)\rangle
   :label: gradientpsis

.. _gradient_cu:

Closed systems, evolution operator propagations
===============================================

Sometimes, the problem is posed in terms of the action of a given time-dependent
perturbation on the whole Hilbert space. In other words, the problem is not optimizing
the system trajectory when it starts from one initial state, but on designing simultaneously
the behaviour of all states. This calls for a formulation of the problem in terms
of the evolution operator:

.. math::
   i\frac{\partial}{\partial t}U(t) = H(u, t)U(t)
   :label: schrodingeru2
.. math::
   U(0) = I
   :label: schuinitstate

The target is then defined in terms of this operator:

.. math::
   F = F(U(T), u)
   :label: targetfunctionU

that is, the maximization of function

.. math::
   G(u) = F(U_u(T), u)
   :label: targetfunctionGU

The expression for the gradient of this function implemented in the code is:

.. math::
   \frac{\partial G}{\partial u_m}(u) =
   \frac{\partial F}{\partial u}(U_u(T), u) +
   2 {\rm Im} \int_0^T\!\!{\rm d}t\; B_u(t) \cdot
   \frac{\partial H}{\partial u_m}(u) U_u(t)
   :label: gradientU

Here, we are using the Fröbenius dot product for operators: :math:`A\cdot B = {\rm Tr}A^\dagger B`.


The costate :math:`B_u(t)` is now defined by:

.. math::
   i\frac{\partial}{\partial t}B_u(t) = H^\dagger(u, t)B_u(t)
   :label: costateU
.. math::
   B_u(T) = \frac{\partial F}{\partial U^*_u(T)}(U_u(T), u)
   :label: costateUinitstate


For example, if the goal is to achieve a certain evolution operator (a *quantum gate*),
we may define a target in the form:

.. math::
   F(U(T), u) = \vert U(T)\cdot U_{\rm target}\vert^2
   :label: targetfunctionUexample

If this is the case, Eq. :math:numref:`costateUinitstate` is:

.. math::
   B_u(T) = (U_{\rm target}\cdot U(T)) U_{\rm target}
   :label: costateUinitstateexample

Once again, the previous setup can be extended to multiple evolution operators
driven by different Hamiltonians.

.. _gradient_od:

Open systems, density matrix propagations
=========================================

Now we consider the problem of optimizing the evolution of an open quantum
system. qocttools only considers one possible model for an open quantum
system: Lindblad's equation:

.. math::
   \dot{\rho}(t) = -i\left[H(u, t), \rho(t)\right] 
   + \sum_{ij} \gamma_{ij} \left( V_{ij}\rho(t)V^\dagger_{ij} - \frac{1}{2}
   \lbrace
   V_{ij}^\dagger V_{ij}, \rho(t)
   \rbrace\right)
   :label: lindblad2

.. math::
   \rho(0) = \rho_0
   :label: lindblad2initialstate

The target function is now a functional of $\rho(T)$:

.. math::
   F = F(\rho(T), u)
   :label: targetfunctionrho

.. math::
   G(u) = F(\rho(T), u)
   :label: targetfuntionGrho

The gradient is given by:

.. math::
   \frac{\partial G}{\partial u_m}(u) =
   \frac{\partial F}{\partial u_m}(\rho_u(T),u) + 
   2{\rm Im} \int_0^T\!{\rm d}t\; {\rm Tr}\left( \sigma^\dagger_u(t) 
   \left[ \frac{\partial H}{\partial u_m}(u, t),  \rho_u(t) \right] \right) \,.
   :label: gradientGrho

where :math:`\rho_u` is the solution density matrix for the :math:`u` control parameters, and
the costate :math:`\sigma_u` density matrix is determined by:

.. math::
   \dot{\sigma}_u(t) = 
   -i \left[ H(u, t), \sigma_u(t)\right]
   -\sum_{ij}\gamma_{ij} \left( V^\dagger_{ij}\sigma_u(t)B_{ij}
   - \frac{1}{2}\lbrace V^\dagger_{ij} V_{ij}, \sigma_u(t)\rbrace\right) 
   :label: costaterho

.. math::
   \sigma_u(T) =
   \frac{\partial F}{\partial \rho^*}(\rho_u(T), u)\,.
   :label: costaterhoinitstate

Note that the dissipative terms for the costate :math:`\sigma` differ from the one
for :math:`\rho` in two ways: first, they have the opposite sign, and second,
the :math:`V_{ij}\rho(t)V_{ij}^\dagger` term is changed by :math:`V_{ij}^\dagger\sigma(t)V_{ij}`.


.. _gradient_floquetc:

Floquet optimizations: closed systems
=====================================

Let us recall that the objective in this case is to optimize a function

.. math::
   f = f(\varepsilon, \phi)\,.
   :label: eq:f2

of the pseudoenergies :math:`\varepsilon` and Floquet modes :math:`\phi`. In fact, at
the moment qocttools only accepts functions of the pseudoenergies: :math:`f=f(\varepsilon)`.
Function :math:`G` is then defined as :math:`G(u) = f(\varepsilon(u))`. There are two possibilities
for the computation of the gradient of his function.

QOCT formula for the evolution operator
---------------------------------------

The first option consists in using the evolution operator in a similar way
to the one described in :numref:`gradient_cu`. One may define a target functional:

.. math::
   F(U) = f(\varepsilon(U))
   :label: eq:targetfunctionalfloquetu

and then, :math:`G(u) = F(U_u)`. The gradient formula would in this case be:

.. math::
   \frac{\partial G}{\partial u_m}(u) =
   2 {\rm Im} \int_0^T\!\!{\rm d}t\; B_u(t) \cdot
   \frac{\partial H}{\partial u_m}(u) U_u(t)
   :label: gradientfloquetU

The costate :math:`B_u(t)` is defined by:

.. math::
   i\frac{\partial}{\partial t}B_u(t) = H^\dagger(u, t)B_u(t)
   :label: floquetcostateU
.. math::
   B_u(T) = \frac{\partial F}{\partial U^*_u(T)}(U_u(T), u)
   :label: floquetcostateUinitstate

Getting the boundary condition at time :math:`t=T` through :math:numref:`floquetcostateUinitstate`
is not trivial, because :math:`F` is not defined directly in terms of the evolution operator,
but indirectly through the pseudoenergies. Currently, the solution to this problem is implemented
in qocttools in simple but probably not too efficient manner: by computing these derivatives
with some finite differences formulas; for example:

.. math::
   \frac{\partial F}{\partial {\rm Re}U_{ij}}
   = \lim_{\Delta \to 0} \Delta^{-1}
   \left[f(\varepsilon_\alpha(U + \Delta \delta_{ij}))
   - f(\varepsilon_\alpha(U))
   \right]\,.
   :label: fdbc


Formula based on perturbation theory
------------------------------------

There is an alternative to the method described previously to optimize
properties of a system periodically driven using the Floquet psuedoenergies.

The gradient of :math:`G(u) = f(\varepsilon(u))` is:

.. math::
   \frac{\partial G}{\partial u_m}(u) = \sum_{\alpha}
   \frac{\partial f}{\partial \varepsilon_\alpha}(\varepsilon(u))
   \frac{\partial \varepsilon_\alpha}{\partial u_m}(u)
   :label: eq:floquetchainrule

The user, that supplies :math:`f(\varepsilon)` (the function that is to be
maximized), should also supply functions for 
the derivatives :math:`\frac{\partial f}{\partial \varepsilon_\alpha}` (some
default options are included in the code). The problem lies then
in the computation of :math:`\frac{\partial \varepsilon_\alpha}{\partial u_m}`.

In order to get those derivatives, one may work in Floquet-Hilbert space.
Modes and psuedoenergies are define there through an eigenvalue equation:

.. math::
    \left[ H(u) -i\frac{\rm d}{{\rm d}t}\right]\vert \phi_\alpha(u)\rangle\rangle = \varepsilon_\alpha(u) 
    \vert \phi_\alpha(u)\rangle\rangle\,.
   :label: eq:modeeigeqv

One may then use perturbation theory in Floquet-Hilbert space and arrive to the
following expression:

.. math::
   \frac{\partial \varepsilon_\alpha}{\partial u_m} = 
   \langle\langle \phi_\alpha(u)\vert \frac{\partial }{\partial u_m}
   \left[H(u) -i \frac{\rm d}{{\rm d}t} \right]\vert \phi_\alpha(u)\rangle\rangle
   = \frac{1}{T}\int_0^T\!\!{\rm d}t\; \langle \phi_\alpha(u; t)\vert
   \frac{\partial H}{\partial u_m}(u; t)\vert \phi_\alpha(u; t)\rangle
   :label: eq:ptformula



.. _gradient_floqueto:

FLoquet optimizations: non-equilibrium steady states
====================================================

Let us now consider an open quantum system, driven by a periodic
perturbation. In general, the system will decay to a state
that shares the same periodicity than the perturbation itself, a
state that is called a non-equilibirum steady state (NESS).

The problem addressed by qocttools is finding the periodic perturbation
whose associated NESS maximizes the (time-averaged) value of some observable.
We briefly summarize here how this is done.

First, let us start by rewriting Lindblad's equation, :math:numref:`lindblad2`,
in a simpler way:

.. math::
   \dot{\rho}(t) = \mathcal{L}(u, t) \rho(t)
   :label: eq:lindbladshort

The NESSs are the periodic solutions to this equation:

.. math::
   \rho_u(t+T) = \rho_u(t)
   :label: eq:nessperiodicity2

We assume that, for each choice of control parameters :math:`u`, there is one
and only one NESS. If we now consider an observable :math:`O`, the goal is the maximization of
function

.. math::
   G(u) = \frac{1}{T}\int_0^T\;\; {\rm Tr}\left[O\rho_u(t)\right]
   :label: guness2

The tasks that qocttools must perform is to compute the NESS :math:`\rho_u`, and
the gradient :math:`\frac{\partial \rho}{\partial u_m}`, in order to be able to 
compute the gradient of :math:`G(u)` as:

.. math::
   \frac{\partial G}{\partial u_m} = \frac{1}{T}\int_0^T\;\; {\rm Tr}
   \left[O\frac{\partial \rho_u}{\partial u_m}(t)\right]
   :label: gradguness

qocttools starts by considering the Fourier series of the elements of the density
matrix and of the Lindbladian operator:

.. math::
   \rho_{\alpha, n} = \frac{1}{T}\int_0^T\!\!{\rm d}t\; e^{-i\omega_n t}\rho_{\alpha}(t)
   :label: eq:rhofs

.. math::
   \mathcal{L}_{\alpha\beta, n}(u) = \frac{1}{T}\int_0^T\!\!{\rm d}t\; e^{-i\omega_n t}\mathcal{L}_{\alpha\beta}(u, t)
   :label: eq:liouvillianfs

Note that :math:`\rho_\alpha(t)` denotes the :math:`\alpha` component of the (time-evolving) density
matrix in vectorized form. Likewise, :math:`\mathcal{L}_{\alpha\beta}(u, t)` is the :math:`\alpha\beta`
component of the Lioviliian operaor (we are dropping the dependence on :math:`u` for clarity).
Lindblad's equation can then be rewritten as:

.. math::
   \sum_\beta \sum_{m=0}^{N-1} \overline{\mathcal{L}}_{\alpha n,\beta m}(u) \rho_{\beta, m} = 0.
   :label: eq:homogeneous

defining

.. math::
   \overline{\mathcal{L}}_{\alpha n,\beta m}(u) = \mathcal{L}_{\alpha\beta,n-m}(u) -i\delta_{nm}\delta_{\alpha\beta}\omega_m,
   :label: eq:defoverlineL

Equation :math:numref:`eq:homogeneous` is used by qocttools to compute the NESS. Then,
by taking variations with respect to :math:`u`, one can compute the gradient
of the NESS density matrix:

.. math::
   \overline{\mathcal{L}}(u)\frac{\partial \rho}{\partial u_k}(u) = 
   - \frac{\partial \overline{\mathcal{L}}}{\partial u_m}(u)\rho_u.
   :label: eq:nessvariation

There are some difficulties related to these equations (since the linear operator is in fact
not invertible. See :cite:p:`Castro2023` for details.



.. _optimization:

Parametrization and Optimization
================================

The control parameters are used to parametrize 
See :class:`pulses.pulse` for the precise definition
of each pulse.

In order to optimize function :math:`G(u)`, one may then use ...

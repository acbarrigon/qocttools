{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f043397d",
   "metadata": {},
   "source": [
    "# Floquet optimization: non-equilibrium steady states (NESSs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "15bb18fd",
   "metadata": {},
   "source": [
    "This tutorial demonstrates a Floquet optimization on a simple three-level quantum model of the Nitrogen-vancancy center in diamond, treated as an open system. The goal is to find a periodic driving that leads to a non-equilibrium steady state (NESS) that is optimal in some given way: in this case, we will define this optimaility in terms of the time-averaged expectation value of some operator."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "821af77b",
   "metadata": {},
   "source": [
    "The model is defined by the three-level Hamiltonian:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "H(u, t) &=& H_0 + V(u, t),\n",
    "\\\\\n",
    "H_0 &=& -B_s S_z + N_z S_z^2 + N_{xy}(S_x^2-S_y^2),\n",
    "\\\\\n",
    "\\label{eq:tdpart}\n",
    "V(u, t) &=& - g_x(u, t) B_d S_x - g_y(u, t) B_d S_y.\n",
    "\\end{eqnarray}\n",
    "\n",
    "This model is taken from [Ikeda *et al*, Science Advances 6, eabb4019 (2020)]. $S_x, S_y$ and $S_z$ are the spin operators, whereas $N_z$, $N_{xy}$, $B_s$, and $B_d$ are constants. The shape of the real time-dependent control functions $g_x$ and $g_y$, dependent on the control parameters $u$, is explained below."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58cc443b",
   "metadata": {},
   "source": [
    "The system is governed by Lindblad's equation:\n",
    "\n",
    "\\begin{align}\n",
    "\\dot{\\rho}(t) = -i\\left[H(u, t), \\rho(t)\\right] \n",
    "+ \\sum_{ij} \\gamma_{ij} \\left( V_{ij}\\rho(t)V^\\dagger_{ij} - \\frac{1}{2}\n",
    "\\lbrace\n",
    "V_{ij}^\\dagger V_{ij}, \\rho(t)\n",
    "\\rbrace\\right)\\,.\n",
    "\\label{eq:lindblad0-eq}\n",
    "\\end{align}\n",
    "\n",
    "The transition operators are $V_{ij} = \\vert E_i\\rangle\\langle E_j\\vert$, and the dissipative constants are $\\gamma_{ij} = \\gamma e^{-\\beta E_i} / (e^{-\\beta E_i}+e^{-\\beta E_j})$ and $\\gamma_{ii}=0$, where $\\beta = 1/(k_{\\rm B}T)$ is the inverse of the temperature, and $\\gamma$ is a rate constant. Notice that this dissipation model ensures the detailed balance condition, $\\gamma_{ij}e^{-\\beta E_j} = \\gamma_{ji}e^{-\\beta E_i}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "468cfb1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import qutip as qt\n",
    "import nlopt\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9785e295",
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.floquet as floquet\n",
    "import qocttools.target as target"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "89787498",
   "metadata": {},
   "source": [
    "It is good practice to print the precise version of the software that you are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c31f089",
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "41ef4367-64ff-4b80-a178-978444b3a32e",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8b2e4b0e",
   "metadata": {},
   "source": [
    "Now, we build the static Hamiltonian $H_0$ (stored into the Qobj object `H0`), and the two coupling operators $V_1 = -B_d S_x$ and $V_2 = -B_d S_y$ (stored into the Qobj objects `V[0]` and `V[1]`). The function `system_definition` also returns some Lindblad operators into the `A` list, as defined above. The field-free eigenvalues are stored stored in the array `e` and the eigenfunctions in `psi`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4f576b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "Sx = qt.jmat(1, \"x\")\n",
    "Sy = qt.jmat(1, \"y\")\n",
    "Sz = qt.jmat(1, \"z\")\n",
    "Bs = 0.3\n",
    "Nz = 1.00\n",
    "Nxy = 0.05\n",
    "Bd = 0.1\n",
    "omega = 1.00\n",
    "gamma = 0.2\n",
    "beta = 3.0\n",
    "d = 3\n",
    "dim = d**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3bd6ccd",
   "metadata": {},
   "outputs": [],
   "source": [
    "def system_definition():\n",
    "    H0 = -Bs * Sz + Nz * Sz**2 + Nxy * (Sx**2 - Sy**2)\n",
    "    Vx = -Bd * Sx\n",
    "    Vy = -Bd * Sy\n",
    "    A = []\n",
    "    e, psi = H0.eigenstates()\n",
    "    for i in range(d):\n",
    "        for j in range(d):\n",
    "            if j == i:\n",
    "                continue\n",
    "            gammaij = gamma * np.exp(-beta*e[j]) / (np.exp(-beta*e[i])+np.exp(-beta*e[j]))\n",
    "            A.append( np.sqrt(gammaij) * psi[j] * psi[i].dag())\n",
    "    return H0, [Vx, Vy], A, e, psi\n",
    "\n",
    "H0, V, A, e, psi = system_definition()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eda017de",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Field-free eigenvalues = {}\".format(e))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3136575d",
   "metadata": {},
   "source": [
    "We must set a value for the period of the driving, and the corresponding frequency. That is of course problem dependent; for this tutorial we will arbitrarily use $\\omega = 0.5$. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b22cd15c",
   "metadata": {},
   "outputs": [],
   "source": [
    "omega0 = 0.5\n",
    "T = (2.0*np.pi/omega0)\n",
    "nts = 100\n",
    "times = np.linspace(0, T, nts + 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b871ccf7",
   "metadata": {},
   "source": [
    "We now build the [hamiltonian](../modules.rst#hamiltonians.hamiltonian) object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "17598496",
   "metadata": {},
   "outputs": [],
   "source": [
    "Ham = hamiltonians.hamiltonian(H0, V, A)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb3b4bd6",
   "metadata": {},
   "source": [
    "The *target operator* is $S_z$. Thus, the goal would be to find the control parameters that lead to a NESS that maximizes the time-average of $\\langle S_z\\rangle$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b08284dc",
   "metadata": {},
   "outputs": [],
   "source": [
    "target_operator = Sz"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9b933b8e",
   "metadata": {},
   "source": [
    "These functions are used to to create the pulses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e1e69b56",
   "metadata": {},
   "outputs": [],
   "source": [
    "def pulse_definition(T, p, bound = 4.0, seed = 0):\n",
    "    if seed >= 0:\n",
    "        np.random.seed(seed)\n",
    "        u = (bound-(-bound)) * np.random.random_sample(p) + (-bound)\n",
    "        g1 = pulses.pulse(\"fourier\", T, u)\n",
    "        u = (bound-(-bound)) * np.random.random_sample(p) + (-bound)\n",
    "        g2 = pulses.pulse(\"fourier\", T, u)\n",
    "    else:\n",
    "        #M = p\n",
    "        K = 1\n",
    "        u = np.zeros(p)\n",
    "        u[K] = bound #np.sqrt(T)/2\n",
    "        g1 = pulses.pulse(\"fourier\", T, u)\n",
    "        u = np.zeros(p)\n",
    "        u[K+1] = bound #np.sqrt(T)/2\n",
    "        g2 = pulses.pulse(\"fourier\", T, u)\n",
    "    return [g1, g2]\n",
    "\n",
    "\n",
    "def pulse_set_new(g, bound = 4.0, seed = 0):\n",
    "    np.random.seed(seed)\n",
    "    p = g[0].u.shape[0]\n",
    "    u = (bound-(-bound)) * np.random.random_sample(p) + (-bound)\n",
    "    g[0].set_parameters(u)\n",
    "    u = (bound-(-bound)) * np.random.random_sample(p) + (-bound)\n",
    "    g[1].set_parameters(u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "40ce9849",
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 4\n",
    "bound = 4.0\n",
    "g = pulse_definition(T, 2*M+1, bound = bound, seed = -1)\n",
    "gref = pulse_definition(T, 2*M+1, bound = bound, seed = -1)\n",
    "u = pulses.pulse_collection_get_parameters(g)\n",
    "pulses.pulse_collection_set_parameters(gref, u)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "819a4c7a",
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('floquet', operator = target_operator, T = T)\n",
    "opt = qoct.Qoct(Ham, T, nts, tg, g, None, floquet_mode = 'ness')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8ca1623",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"G(u) = {}\".format(opt.gfunc(u)))\n",
    "print(\"G(u=0) = {}\".format(opt.gfunc(np.zeros_like(u))))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "914c0105",
   "metadata": {},
   "source": [
    "We will not do the optimization, because we want the tutorial to run quickly. But let us compute the gradient of the target function, and check it against a finite-difference formula."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "088c8dbd",
   "metadata": {},
   "outputs": [],
   "source": [
    "check_gradient = True\n",
    "if check_gradient:\n",
    "    derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "    print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "    print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a9249fae-6974-491d-9e9b-39dac047bdec",
   "metadata": {},
   "outputs": [],
   "source": [
    "data.append(derqoct)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2cbededa-da07-433f-99a7-ea7e010f0253",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This file is used by the testing script of the code.\n",
    "with open(\"data\", \"w\") as f:\n",
    "    for i in data:\n",
    "        f.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

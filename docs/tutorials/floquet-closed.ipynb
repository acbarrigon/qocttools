{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f043397d",
   "metadata": {},
   "source": [
    "# Floquet optimization on a closed system"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cc17c0e9",
   "metadata": {},
   "source": [
    "This tutorial demonstrates a Floquet optimization on a simple three-level quantum model of the Nitrogen-vancancy center in diamond. The goal is to find a periodic driving that modifies the Floquet pseudoenergies of the system, in such a way that a certain function of these pseudoenergies is maximized."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a6070c4",
   "metadata": {},
   "source": [
    "What we will do is to define a *reference* periodic driving, that leads to a set of reference pseudoenergies, and then do an optimization that attempts to find another periodic driving that leads to the same pseudoenergies (of course, the found optimal driving could be the same as the reference one, although in this case this is not what happens). In order to achieve this goal, we define a function of the pseudoenergies that measures the distance to the reference ones."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e4aa9592",
   "metadata": {},
   "source": [
    "The model is defined by the three-level Hamiltonian:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "H(u, t) &=& H_0 + V(u, t),\n",
    "\\\\\n",
    "H_0 &=& -B_s S_z + N_z S_z^2 + N_{xy}(S_x^2-S_y^2),\n",
    "\\\\\n",
    "\\label{eq:tdpart}\n",
    "V(u, t) &=& - g_x(u, t) B_d S_x - g_y(u, t) B_d S_y.\n",
    "\\end{eqnarray}\n",
    "\n",
    "This model is taken from [Ikeda *et al*, Science Advances 6, eabb4019 (2020)]. $S_x, S_y$ and $S_z$ are the spin operators, whereas $N_z$, $N_{xy}$, $B_s$, and $B_d$ are constants. The shape of the real time-dependent control functions $g_x$ and $g_y$, dependent on the control parameters $u$, is explained below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "468cfb1b",
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import numpy as np\n",
    "import qutip as qt\n",
    "import nlopt\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "408d675c",
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.floquet as floquet\n",
    "import qocttools.target as target"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0d7139d7",
   "metadata": {},
   "source": [
    "It is good practice to print the precise version of the software that you are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1c31f089",
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a996087c-f1d9-42b8-a50b-35ca560dd6fc",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5a4bcbcf",
   "metadata": {},
   "source": [
    "Now, we build the static Hamiltonian $H_0$ (stored into the Qobj object `H0`), and the two coupling operators $V_1 = -B_d S_x$ and $V_2 = -B_d S_y$ (stored into the Qobj objects `V[0]` and `V[1]`). The function `system_definition` in principle returns some Lindblad operators, but those are not used in this tutorial. The field-free eigenvalues are stored stored in the array `e` and the eigenfunctions in `psi`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f4f576b3",
   "metadata": {},
   "outputs": [],
   "source": [
    "Sx = qt.jmat(1, \"x\")\n",
    "Sy = qt.jmat(1, \"y\")\n",
    "Sz = qt.jmat(1, \"z\")\n",
    "Bs = 0.3\n",
    "Nz = 1.00\n",
    "Nxy = 0.05\n",
    "Bd = 0.1\n",
    "omega = 1.00\n",
    "gamma = 0.2\n",
    "beta = 3.0\n",
    "d = 3\n",
    "dim = d**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d3bd6ccd",
   "metadata": {},
   "outputs": [],
   "source": [
    "def system_definition():\n",
    "    H0 = -Bs * Sz + Nz * Sz**2 + Nxy * (Sx**2 - Sy**2)\n",
    "    Vx = -Bd * Sx\n",
    "    Vy = -Bd * Sy\n",
    "    A = []\n",
    "    e, psi = H0.eigenstates()\n",
    "    for i in range(d):\n",
    "        for j in range(d):\n",
    "            if j == i:\n",
    "                continue\n",
    "            gammaij = gamma * np.exp(-beta*e[j]) / (np.exp(-beta*e[i])+np.exp(-beta*e[j]))\n",
    "            A.append( np.sqrt(gammaij) * psi[j] * psi[i].dag())\n",
    "    return H0, [Vx, Vy], A, e, psi\n",
    "\n",
    "H0, V, A, e, psi = system_definition()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "eda017de",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"Field-free eigenvalues = {}\".format(e))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "628c6062",
   "metadata": {},
   "source": [
    "We must set a value for the period of the driving, and the corresponding frequency. That is of course problem dependent; for this tutorial we will arbitrarily use $\\omega = 3$. In this way, the Floquet-Brillouin zone is set to be $[-1.5, 1.5)$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "277ea406",
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = 3.0\n",
    "T = (2.0*np.pi/omega)\n",
    "print(\"The periodic driving frequency is {} eV.\".format(omega))\n",
    "print(\"The Floquet period is {} a.u\".format(T))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "43b7d2e6",
   "metadata": {},
   "source": [
    "We will first define some *reference* periodic driving: a circularly polarized field with a single frequency, `omega`. But we will first do a zero-field calculation, setting the amplitude to zero, which should yield some pseudoenergies that are equal to the system eigenvalues. In order to compute the pseudoenergies, we will use\n",
    "the [epsilon()](../modules.rst#floquet.epsilon) function from the floquet module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a3ee7d20",
   "metadata": {},
   "outputs": [],
   "source": [
    "a0 = 0.0\n",
    "\n",
    "def Axref(t, args):\n",
    "    return a0 * np.sin(omega * t)\n",
    "\n",
    "def Ayref(t, args):\n",
    "    return a0 * np.cos(omega * t)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9587964a",
   "metadata": {},
   "source": [
    "This is one way to specify a time-dependent Hamiltonian when using qutip (see the qutip documentation for details). This is the format that the function [epsilon()](../modules.rst#floquet.epsilon) expects."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8a387b22",
   "metadata": {},
   "outputs": [],
   "source": [
    "H = [H0, [V[0], Axref], [V[1], Ayref]]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5cfba5f9",
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon0 = floquet.epsilon(H, T)\n",
    "print(\"Field-free Floquet pseudoenergies = {}\".format(epsilon0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e3c53bd",
   "metadata": {},
   "source": [
    "As it can be seen, the pseudoenergies are equal to the energies. Let us now redo the calculation, but using a non-zero amplitude."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76b6181b",
   "metadata": {},
   "outputs": [],
   "source": [
    "a0 = 5.0"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "82ecf1e2",
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon_ref = floquet.epsilon(H, T)\n",
    "print(\"Reference Floquet pseudoenergies = {}\".format(epsilon_ref))\n",
    "print(\"Field-free Floquet pseudoenergies = {}\".format(epsilon0))\n",
    "print(\"Diff with the field-free eigenvalues = {}\".format(epsilon_ref-epsilon0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f8e701e",
   "metadata": {},
   "source": [
    "The pseudoenergies in `epsilon_ref` are now the target pseudoenergies."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "556c9437",
   "metadata": {},
   "source": [
    "We will now define the parametrized pulses (Fourier series, in this example), whose parameters will be used for the optimization. In the code below, we have the option of reading the pulses from a file (if the pulses have been stored in a file in a previous run), generating a random pulse, or using some predefined values for the parameters. The last thing is what we do in the tutorial -- although, in fact, the values were previously randomly generated; we hardcode them here in order to ensure that the tutorial always produces the same numbers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5fbb0a86",
   "metadata": {},
   "outputs": [],
   "source": [
    "times = np.linspace(0, T, 100)\n",
    "maxamp = a0 * np.sqrt(T) / 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f142722f",
   "metadata": {},
   "outputs": [],
   "source": [
    "read_initial_guess_from_disk = False"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "45295c42",
   "metadata": {},
   "outputs": [],
   "source": [
    "if not read_initial_guess_from_disk:\n",
    "    \n",
    "    M = 5\n",
    "    random_initial_pulse = False\n",
    "\n",
    "    if random_initial_pulse:\n",
    "        random_bound = 1.0 * maxamp\n",
    "        u1 = np.zeros(2*M+1)\n",
    "        u1[1:] = np.random.uniform(low = -1.0, high = 1.0, size = 2*M)\n",
    "        u1[1:] = random_bound * u1[1:]\n",
    "        u2 = np.zeros(2*M+1)\n",
    "        u2[1:] = np.random.uniform(low = -1.0, high = 1.0, size = 2*M)\n",
    "        u2[1:] = random_bound * u2[1:]\n",
    "    else:\n",
    "        # These numbers were random-generated. We hard-code them here, to ensure the\n",
    "        # exact reproducibility of the tutorial results.\n",
    "        u1 = np.array([0.0, 0.3781672550042017, 3.337578192688232,\n",
    "                       0.39792661221249986, 2.640586714801746, \n",
    "                       1.275967487033285, -1.0266663277960986,\n",
    "                       1.690341853324815, -2.7232255567616743,\n",
    "                       -0.20406687253848577, 1.020998297311514])\n",
    "        u2 = np.array([0.0, -0.8765685785388411, 3.059796305834288,\n",
    "                       -1.3956891274241654, 2.931174340624856,\n",
    "                       -2.03904569878065, -1.6616521056176727,\n",
    "                       3.3729352354374806, -1.0565599367266927,\n",
    "                       2.7168660074286004, -0.229243896077151])\n",
    "    \n",
    "    Ax0 = pulses.pulse(\"fourier\", T, u = u1)\n",
    "    Ay0 = pulses.pulse(\"fourier\", T, u = u2)\n",
    "    Axopt = pulses.pulse(\"fourier\", T, u = u1)\n",
    "    Ayopt = pulses.pulse(\"fourier\", T, u = u2)\n",
    "    Ax0.print('Ax0')\n",
    "    Ay0.print('Ay0')\n",
    "else:\n",
    "    Ax0 = pulses.read_pulse('Ax0')\n",
    "    Ay0 = pulses.read_pulse('Ay0')\n",
    "    Axopt = pulses.read_pulse('Ax0')\n",
    "    Ayopt = pulses.read_pulse('Ay0')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "35a6f24b",
   "metadata": {},
   "source": [
    "Let us make a plot of the initial-guess for the drivings:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4fa64be9",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(5,4))\n",
    "ax = fig.add_axes([0.15, 0.15, 0.8, 0.8])\n",
    "\n",
    "times = np.linspace(0, T, 100)\n",
    "\n",
    "ax.plot(times/T, Axref(times, None), label = r\"$A^{\\rm ref}_x(t)$\")\n",
    "ax.plot(times/T, Ayref(times, None), label = r\"$A^{\\rm ref}_y(t)$\")\n",
    "\n",
    "ax.plot(times/T, Ax0.fu(times), label = r\"$A^{\\rm 0}_x(t)$\")\n",
    "ax.plot(times/T, Ay0.fu(times), label = r\"$A^{\\rm 0}_y(t)$\")\n",
    "\n",
    "ax.legend()\n",
    "ax.set_xlim(left = 0, right = 1)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "87bbdded",
   "metadata": {},
   "source": [
    "The variable `u0` holds *all* the parameters, from both pulses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9fdf665a",
   "metadata": {},
   "outputs": [],
   "source": [
    "u0 = pulses.pulse_collection_get_parameters([Ax0, Ay0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5cff86bb",
   "metadata": {},
   "source": [
    "We now need to create a [hamiltonian](../modules.rst#hamiltonians.hamiltonian) object."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c6d1d7e",
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, V)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b9dfd680",
   "metadata": {},
   "source": [
    "Let us now compute the pseudo-energies with the parameters of the initial-guess pulses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "97d6538e",
   "metadata": {},
   "outputs": [],
   "source": [
    "u = u0.copy()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3fdd5d75",
   "metadata": {},
   "outputs": [],
   "source": [
    "epsilon_ig = floquet.epsilon3(H, [Ax0, Ay0], u, T)\n",
    "print(\"Initial-guess Floquet pseudoenergies = {}\".format(epsilon_ig))\n",
    "print(\"Reference Floquet pseudoenergies = {}\".format(epsilon_ref))\n",
    "print(\"Diff = {}\".format(epsilon_ig-epsilon_ref))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4864a4b",
   "metadata": {},
   "source": [
    "Next step is the definition of the target function $f(\\varepsilon)$, and its gradient. This is done in the following cell. The definition is:\n",
    "\\begin{equation}\n",
    "f(\\varepsilon) = -\\sum_\\alpha (\\varepsilon_\\alpha - \\varepsilon^{\\rm ref}_\\alpha)^2\n",
    "\\end{equation}\n",
    "The maximum of this function is achieved when $\\varepsilon = \\varepsilon^{\\rm ref}$, and then $f(\\varepsilon)=0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f58f3bb8",
   "metadata": {},
   "outputs": [],
   "source": [
    "targeteps = epsilon_ref.reshape(1, 3)\n",
    "\n",
    "def f(eps):\n",
    "    cte = 1.0\n",
    "    fval = 0.0\n",
    "    nkpoints = eps.shape[0]\n",
    "    targete = targeteps\n",
    "    dim = eps.shape[1]\n",
    "    fval = 0.0\n",
    "    for k in range(nkpoints):\n",
    "        for alpha in range(dim):\n",
    "            fval = fval - cte * (eps[k, alpha] - targete[k, alpha])**2\n",
    "    return fval\n",
    "    \n",
    "def dfdepsilon(eps):\n",
    "    cte = 1.0\n",
    "    nkpoints = eps.shape[0]\n",
    "    targete = targeteps\n",
    "    dim = eps.shape[1]\n",
    "    dfval = np.zeros((nkpoints, dim))\n",
    "    for k in range(nkpoints):\n",
    "        for alpha in range(dim):\n",
    "            dfval[k, alpha] = - 2.0 * cte * (eps[k, alpha]-targete[k, alpha])\n",
    "    return dfval"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ea7ca30b",
   "metadata": {},
   "source": [
    "## Computation based on perturbation theory in Floquet-Hilbert space"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "81536867",
   "metadata": {},
   "source": [
    "First, let us do the optimization using the expression from the gradient based on perturbation theory in Floquet-Hilbert space, as described [here](../method.rst#Formula-based-on-perturbation-theory)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8c510b60",
   "metadata": {},
   "outputs": [],
   "source": [
    "u = u0.copy()\n",
    "pulses.pulse_collection_set_parameters([Axopt, Ayopt], u)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7f2d9244",
   "metadata": {},
   "source": [
    "We define the [target](../modules.rst#target.Target) object. Note that we need to tell qocttools about function\n",
    "$f$ and its gradient, defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6c0a5751",
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('floquet', targeteps = targeteps,\n",
    "                   T = T, fepsilon = f, dfdepsilon = dfdepsilon)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "152ecd25",
   "metadata": {},
   "source": [
    "Now, the [Qoct](../modules.rst#qoct.Qoct) object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "cbc84d76",
   "metadata": {},
   "outputs": [],
   "source": [
    "U0set = [qt.qeye(3)]\n",
    "opt = qoct.Qoct(H, T, times.shape[0], tg, [Axopt, Ayopt], U0set, floquet_mode = 'pt')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a0ee16af",
   "metadata": {},
   "source": [
    "Let us look at the value of the target function with the initial-guess parameters:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "dcf13e43",
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"G(u) = {} (initial guess)\".format(opt.gfunc(u)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "26b37a9d",
   "metadata": {},
   "source": [
    "We now do the optimization."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "78fda933",
   "metadata": {},
   "outputs": [],
   "source": [
    "check_gradient = True\n",
    "if check_gradient:\n",
    "    derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "    print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "    print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "\n",
    "optimize = True\n",
    "\n",
    "if optimize:\n",
    "    x, optval, res = opt.maximize(maxeval = 100,\n",
    "                                  verbose = True,\n",
    "                                  algorithm = nlopt.LD_SLSQP,\n",
    "                                  upper_bounds = 1 * np.abs(maxamp * np.ones_like(u)),\n",
    "                                  lower_bounds = -1 * np.abs(maxamp * np.ones_like(u)))\n",
    "    uopt = pulses.pulse_collection_get_parameters([Axopt, Ayopt])\n",
    "    print(opt.gfunc(uopt))\n",
    "    epsilon_opt1 = floquet.epsilon3(H, [Axopt, Ayopt], uopt, T)\n",
    "    print(\"Optimized Floquet pseudoenergies = {}\".format(epsilon_opt1))\n",
    "    print(\"Reference Floquet pseudoenergies = {}\".format(epsilon_ref))\n",
    "    print(\"Diff = {}\".format(epsilon_opt1-epsilon_ref))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "30f7143f",
   "metadata": {},
   "source": [
    "The optimization is successfull (we reach the absolute maximum value of zero). How does the optimal driving compare with the refernce one? We can look at both in the following plot:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f2b8c367",
   "metadata": {},
   "outputs": [],
   "source": [
    "fig = plt.figure(figsize=(5,4))\n",
    "ax = fig.add_axes([0.15, 0.15, 0.8, 0.8])\n",
    "\n",
    "times = np.linspace(0, T, 100)\n",
    "\n",
    "ax.plot(times/T, Axref(times, None), label = r\"$A^{\\rm ref}_x(t)$\")\n",
    "ax.plot(times/T, Ayref(times, None), label = r\"$A^{\\rm ref}_y(t)$\")\n",
    "\n",
    "ax.plot(times/T, Axopt.fu(times), label = r\"$A^{\\rm opt}_x(t)$\")\n",
    "ax.plot(times/T, Ayopt.fu(times), label = r\"$A^{\\rm opt}_y(t)$\")\n",
    "\n",
    "ax.legend()\n",
    "ax.set_xlim(left = 0, right = 1)\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "53dccad8",
   "metadata": {},
   "source": [
    "Surprisingly (or perhaps not so much), the fields are different. Different drivings can produce the same pseudoenergies."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0a31f1b1",
   "metadata": {},
   "source": [
    "## Computation based on a QOCT formula"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1aff30fa",
   "metadata": {},
   "source": [
    "Now, let us do the same optimization, but using the expression from the gradient based on QOCT, as described [here](../method.rst#QOCT-formula-for-the-evolution-operator)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f9f24746",
   "metadata": {},
   "outputs": [],
   "source": [
    "u = u0.copy()\n",
    "pulses.pulse_collection_set_parameters([Axopt, Ayopt], u)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d2796c55",
   "metadata": {},
   "source": [
    "We define the [target](../modules.rst#target.Target) object. Note that we need to tell qocttools about function\n",
    "$f$ and its gradient, defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "780f97c8",
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('floquet', targeteps = targeteps,\n",
    "                   T = T, fepsilon = f, dfdepsilon = dfdepsilon)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b2c554a",
   "metadata": {},
   "source": [
    "Now, the [Qoct](../modules.rst#qoct.Qoct) object. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "8510c992",
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, times.shape[0], tg, [Axopt, Ayopt], U0set, floquet_mode = 'qoct')\n",
    "print(\"G(u) = {} (initial guess)\".format(opt.gfunc(u)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "134b8f1f",
   "metadata": {},
   "source": [
    "And finally, the optimization"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "36064c59",
   "metadata": {},
   "outputs": [],
   "source": [
    "check_gradient = True\n",
    "if check_gradient:\n",
    "    #u = pulses.pulse_collection_get_parameters([Axopt, Ayopt])\n",
    "    derqoct, dernum, error, elapsed_time = opt.check_grad(u)\n",
    "    print(\"QOCT calculation: \\t{}\".format(derqoct))\n",
    "    print(\"Ridders calculation: \\t{} +- {}\".format(dernum, error))\n",
    "    data.append(derqoct)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "4628e20a",
   "metadata": {},
   "outputs": [],
   "source": [
    "optimize = True\n",
    "\n",
    "if optimize:\n",
    "    x, optval, res = opt.maximize(maxeval = 100,\n",
    "                                  verbose = True,\n",
    "                                  #tolerance = -1.0,\n",
    "                                  #algorithm = nlopt.LD_MMA,\n",
    "                                  algorithm = nlopt.LD_SLSQP,\n",
    "                                  #algorithm = nlopt.LN_BOBYQA,\n",
    "                                  #algorithm = nlopt.GD_STOGO,\n",
    "                                  #algorithm = nlopt.LD_LBFGS,\n",
    "                                  upper_bounds = 1 * np.abs(maxamp * np.ones_like(u)),\n",
    "                                  lower_bounds = -1 * np.abs(maxamp * np.ones_like(u)))\n",
    "    uopt = pulses.pulse_collection_get_parameters([Axopt, Ayopt])\n",
    "    print(opt.gfunc(uopt))\n",
    "    epsilon_opt2 = floquet.epsilon3(H, [Axopt, Ayopt], uopt, T)\n",
    "    print(\"Optimized Floquet pseudoenergies = {}\".format(epsilon_opt2))\n",
    "    print(\"Reference Floquet pseudoenergies = {}\".format(epsilon_ref))\n",
    "    print(\"Diff = {}\".format(epsilon_opt2-epsilon_ref))\n",
    "    data.append(optval)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2d38bc5b",
   "metadata": {},
   "source": [
    "Since, after all, we are just using two alternative methods to compute the same gradient, we get the same result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a24ae658-0d24-422e-82b5-8f2ddc25afae",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This file is used by the testing script of the code.\n",
    "with open(\"data\", \"w\") as f:\n",
    "    for i in data:\n",
    "        f.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

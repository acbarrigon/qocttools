{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "2a7ad304-69be-47dc-8a12-e50030b04c7e",
   "metadata": {},
   "source": [
    "# Optimization on a closed system: creation of the CNOT gate in a two-qubit system"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c13d6d1-c881-42c8-adf8-468db16e86ad",
   "metadata": {},
   "source": [
    "This tutorial demonstrates the design of pulses for the creation of a CNOT gate in a two-qubit system. This is done using both qocttools and, for the sake of comparison, the GRAPE implementation of QuTiP.\n",
    "\n",
    "The model for the 2-qubit system that we will use is the following: the static Hamiltonian is given by:\n",
    "\\begin{equation}\n",
    "H_0 = \\frac{1}{2}\\omega_1 \\sigma_z \\otimes I_2 + \\frac{1}{2}\\omega_2 I_2 \\otimes \\sigma_z\\,,\n",
    "\\end{equation}\n",
    "whereas we will assume that one can use the two following perturbations, that permit to control the system and implement the CNOT gate:\n",
    "\\begin{align}\n",
    "V_1 &= \\sigma_x \\otimes \\sigma_x\\,,\n",
    "\\\\\n",
    "V_2 &= I_2 \\otimes \\sigma_x + \\sigma_x \\otimes I_2\\,.\n",
    "\\end{align}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e10b55be-05e8-4cb3-82c9-493ec6db27e3",
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib\n",
    "from copy import deepcopy\n",
    "import matplotlib.pyplot as plt\n",
    "import qutip as qt\n",
    "# WARNING: The use of the quantum control implementation in QuTiP is commented out\n",
    "# in this tutorial, as there seems to be a problem in the prerelease version of QuTiP 5.\n",
    "#import qutip_qtrl.pulseoptim as cpo\n",
    "import qutip_qip.operations as operations\n",
    "import datetime\n",
    "import nlopt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "1fbaeaa6-7e19-4c4a-8285-53bfa32e0561",
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.target as target\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.solvers as solvers"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d886ce37-52d3-4a8f-9d8e-a08d2c055c57",
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "804638c5-0cd9-4e4b-9b45-ee46d89cd736",
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "73000f55-8600-4c14-b03b-b0b25cd45e82",
   "metadata": {},
   "source": [
    "We start by defining the static or \"drift\" Hamiltonian ``H0``. We will use a qubit with frequency $\\omega_1 = 1.0$, and another one with different frequency $\\omega_2 = 2.0$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "35292374-ecf6-479b-99eb-ec8fda6a700c",
   "metadata": {},
   "outputs": [],
   "source": [
    "w1 = 1.0\n",
    "w2 = 2.0\n",
    "h01 = 0.5 * w1 * qt.tensor(qt.sigmaz(), qt.qeye(2))\n",
    "h02 = 0.5 * w2 * qt.tensor(qt.qeye(2), qt.sigmaz())\n",
    "H0 = h01 + h02\n",
    "# To use in qocttools, we need to eliminate the internal tensor structure of the QuTiP object,\n",
    "# and use simple 1-1 tensors as operators.\n",
    "H0 = qt.Qobj(H0, dims = [[4], [4]])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e13a1368-d272-41fc-a784-1d70690beb21",
   "metadata": {},
   "source": [
    "Now we define the two perturbations that we will use."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "26e02f8a-7a08-404a-99d6-e129b9d7f0b7",
   "metadata": {},
   "outputs": [],
   "source": [
    "V1 = qt.tensor(qt.sigmax(), qt.sigmax())\n",
    "V2 = qt.tensor(qt.sigmax(), qt.qeye(2)) + qt.tensor(qt.qeye(2), qt.sigmax())\n",
    "V1 = qt.Qobj(V1, dims = [[4], [4]])\n",
    "V2 = qt.Qobj(V2, dims = [[4], [4]])\n",
    "V = [V1, V2]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ceea46fb-2e9a-408b-a6f0-857505bf2914",
   "metadata": {},
   "source": [
    "The CNOT gate is defined in QuTiP, although we have to reshape the object to be used in qocttools"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3ab1fa7c-fe15-49ba-b134-d239bbb8e9b9",
   "metadata": {},
   "outputs": [],
   "source": [
    "UCNOT = qt.Qobj(operations.cnot(), dims = [[4], [4]])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "22d8c1fc-f403-441a-bc5c-658e6b6362e0",
   "metadata": {},
   "source": [
    "For the total propagation time $T$, we will use five times the period associated to the frequency of the first qubit. We will also define $\\omega_0 = \\frac{2\\pi}{T}$ as the base frequency for the Fourier expansion."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f60af171-65e8-4324-bf88-0abdb2d06cf0",
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = w1\n",
    "tau = 2.0*np.pi/omega\n",
    "ncycles = 5\n",
    "T = ncycles * tau\n",
    "# omega0 will be the base frequency for the Fourier expansion\n",
    "omega0 = 2.0*np.pi/T\n",
    "print(\"omega0 = {:.4f}\".format(omega0))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "eebaecb7-4192-4e34-86fb-29b8d1232ee3",
   "metadata": {},
   "source": [
    "## Optimization with qocttools"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "40fd2d1b-6411-4f12-8fa4-dbaf5456d623",
   "metadata": {},
   "source": [
    "Let us perform the calculations with qocttools. We start by building the [hamiltonian](../modules.rst#hamiltonians.hamiltonian) object:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f69668ca-4f22-4515-9f05-b318eace6d51",
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, V)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9570b160-75ee-4fe2-a5df-cc8171d0cbb4",
   "metadata": {},
   "source": [
    "Next, we create the [Target](../modules.rst#target.Target). The type of target in this case is a \"gate\" or evolution operator:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "81e84e77-cf2c-487c-8886-70de232037fd",
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target(\"evolutionoperator\", Utarget = UCNOT)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "38695c37-d059-4b0b-9f5a-1fefa09814c4",
   "metadata": {},
   "source": [
    "Now, we must define the time-dependent functions that will be used as controls; the total Hamiltonian will be given by:\n",
    "\\begin{equation}\n",
    "H(u, t) = H_0 + f_1(u^{(1)}, t)V_1 + f_2(u^{(2)}, t) V_2\\,.\n",
    "\\end{equation}\n",
    "where $u$ represents all the control parameters, and is composed of two sets of parameters, $u=(u^{(1)}, u^{(2)})$, one for each control function. We will use a normal Fourier parametrization for the control functions, and therefore those parameters are the coefficients of the Fourier expansions. See the documentation on the [pulse](../modules.rst#pulses.pulse) class for details."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cd8e2d67-93bb-40cb-b9fd-3c1bbe9f5fee",
   "metadata": {},
   "source": [
    "First, we will define some initial values for the control parameters. The first step is to set the cutoff frequency; the Fourier expansion will be made up of the frequencies $\\omega_i = i\\omega_0$ for $i=0,1,2,\\dots M$. Therefore, one has to choose a value for $M$.\n",
    "\n",
    "Then, in order to choose the initial control parameter values, one often wants to use random ones. Here, in order to ensure the reproducibility of the tutorial, we will reuse some values that were previously randomly generated. Note that, in order to generate those random numbers, one needs to set an amplitude bound, i.e. $\\vert u_k\\vert \\le \\kappa$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7bc40b9c-9631-49f3-a8a9-72bd430d44fe",
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 10 # The cutoff index of the Fourier expansion\n",
    "kappa = 0.1 # The amplitude bound for the initial random values.\n",
    "\n",
    "# The following code sets random numbers for the control parameters\n",
    "#bound = kappa\n",
    "#a = -bound\n",
    "#b = bound\n",
    "#u1 = (b-a) * np.random.rand(2*M+1) + a\n",
    "#u2 = (b-a) * np.random.rand(2*M+1) + a\n",
    "#print(repr(u1))\n",
    "#print(repr(u2))\n",
    "\n",
    "# But instead, we will hardcode here values that were generated before.\n",
    "u1 = np.array([-0.03203772,  0.08398405, -0.07384907, -0.06540511,  0.04327422,\n",
    "       -0.03974848, -0.06372874,  0.01909529,  0.09770391, -0.00169402,\n",
    "       -0.0471008 , -0.05903078, -0.09347964, -0.02234591, -0.0223768 ,\n",
    "        0.09680869,  0.04258134, -0.05684655, -0.01742488, -0.00879573,\n",
    "       -0.01601734])\n",
    "u2 = np.array([ 0.05399459,  0.08808636, -0.00589051, -0.00989415,  0.03005704,\n",
    "        0.07721242, -0.0536454 ,  0.03674837, -0.03142538,  0.07709192,\n",
    "       -0.03038883, -0.06144648,  0.0870775 , -0.01474247,  0.06243688,\n",
    "        0.01285235, -0.08496945, -0.08987118,  0.02714249, -0.01303071,\n",
    "        0.00023745])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1cb4a3e-d8bd-41bf-a780-34d5bc4a3fa1",
   "metadata": {},
   "source": [
    "We can now create the two pulse objects, that we place into a list:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a07d1b31-7035-4e30-802f-821c6503f30e",
   "metadata": {},
   "outputs": [],
   "source": [
    "f = [pulses.pulse(\"fourier\", T, u1), pulses.pulse(\"fourier\", T, u2)]\n",
    "f0 = deepcopy(f) # We create a copy to store the initial values\n",
    "# u0 will store the whole collection of initial parameters of the two pulses\n",
    "u0 = pulses.pulse_collection_get_parameters(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4a180bf0-051a-4990-a253-2914facf0120",
   "metadata": {},
   "source": [
    "Now we create the [Qobj](../modules.rst#qoct.Qoct) object. We need to specify the number of steps in the time discretization that is used to do the propagations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "75bac8f1-a9e7-4e97-b25c-784481c094cd",
   "metadata": {},
   "outputs": [],
   "source": [
    "ntsteps = ncycles * 20\n",
    "U0 = qt.identity(4)\n",
    "opt = qoct.Qoct(H, T, ntsteps, tg, f, U0,\n",
    "                interaction_picture = False)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e2e23ef1-681b-45d3-8c42-1adaf957e387",
   "metadata": {},
   "source": [
    "Before doing the optimization itself, let us do an initial propagation to see how the initial guess pulses perform. We compute the value of the target function along the propagation. In this case where the target is the creation of a gate, the definition of the target function is:\n",
    "\\begin{equation}\n",
    "F(U) = \\frac{1}{{\\rm dim}^2} \\vert {\\rm Tr} U^\\dagger U_{\\rm target} \\vert^2\n",
    "\\end{equation}\n",
    "In our case, the target gate is the CNOT gate, and ${\\rm dim}=4$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3301b9d6-2256-4946-bdc6-6297e6ab6b70",
   "metadata": {},
   "outputs": [],
   "source": [
    "def F(U):\n",
    "    return (1/4**2) * np.abs((UCNOT.dag() * res[j]).tr())**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "72a7f569-339e-4bf6-906c-8973cff09ca2",
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, qt.qeye(4), ts)\n",
    "Ftinitial = np.zeros(ntsteps)\n",
    "for j in range(ntsteps):\n",
    "    Ftinitial[j] = F(res[j])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ccdd4e94-003d-4294-90a7-d17b29e3adff",
   "metadata": {},
   "source": [
    "Now we can launch the optimization. In this case, we will use the ``LBFGS`` algorithm from the nlopt library (in principle, one can use any of the algorithms defined there). We will set a maximum of 100 function evaluations as a limit to stop the run, and we will also ask the algorithm to stop whenever a ``stopval`` value is reached, that we set to 0.99 (ideally, a full overlap with the target gate is achieved when the target function is one). See the documentation for the [maximize](../modules.rst#qoct.Qoct.maximize) method for more details and options."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "bf1094c1-686a-4c78-84b7-18c54d82158d",
   "metadata": {},
   "outputs": [],
   "source": [
    "# Before the optimization, one could do a check on the quality of the gradient with this call:\n",
    "#derqoct, dernum, error = opt.check_grad(u0)\n",
    "#print(derqoct, dernum, error)\n",
    "\n",
    "x, optval, result = opt.maximize(algorithm = nlopt.LD_LBFGS,\n",
    "                                 maxeval = 100,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True)\n",
    "data.append(optval)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ac0de3d-d7a6-4eb9-87fb-15ca237d7c00",
   "metadata": {},
   "source": [
    "Let us check how the optimal pulse does. The optimal parameters are stored in the ``x`` array. The following updates the list of pulses ``f`` with those values:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "6f06920f-89ed-4705-9937-b42073ef1670",
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(f, x)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5492d88f-b729-4904-be8c-805c20642d7c",
   "metadata": {},
   "source": [
    "Now we can re-propagate with the optimized pulses:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7ab8f925-f702-4f49-b153-698d32310569",
   "metadata": {},
   "outputs": [],
   "source": [
    "res = solvers.solve('cfmagnus4', H, f, qt.qeye(4), ts)\n",
    "Ft = np.zeros(ntsteps)\n",
    "for j in range(ntsteps):\n",
    "    Ft[j] = F(res[j])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bb1458ac-1e7a-465a-bea4-5641ed00ef16",
   "metadata": {},
   "source": [
    "And finally, we plot the results. In the top panel, the value of the target function as it evolves in time, for the initial guess and for the optimal pulses. In the bottom panel, those pulses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f94e9a6a-cd0d-4c2a-bf55-c0e4f248ca30",
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"font.size\"] = \"8\"\n",
    "plt.rcParams[\"figure.autolayout\"] = False\n",
    "\n",
    "fig, ax = plt.subplots(2, 1, figsize=(5, 5), sharex = True)\n",
    "fig.subplots_adjust(hspace=0)\n",
    "\n",
    "ax[0].plot(ts/tau, Ftinitial, label = r\"$F(U_{\\rm initial}(t))$\")\n",
    "ax[0].plot(ts/tau, Ft, label = r\"$F(U(t))$\")\n",
    "#ax[0].set_xlabel(r\"$t/\\tau$\")\n",
    "ax[0].set_xlim(0, T/tau)\n",
    "ax[0].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "ts = np.linspace(0, T, ntsteps)\n",
    "\n",
    "ax[1].plot(ts/tau, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f[0].fu(ts), label = r\"$g_x^{\\rm opt}(t)$\")\n",
    "ax[1].plot(ts/tau, f[1].fu(ts), label = r\"$g_y^{\\rm opt}(t)$\")\n",
    "ax[1].set_xlabel(r\"$t/\\tau$\")\n",
    "ax[1].set_xlim(0, T/tau)\n",
    "#ax[1].label_outer()\n",
    "ax[1].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "#fig.savefig(\"closed-gate.pdf\", bbox_inches = 'tight')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cceaa7b5-711a-498e-a297-00ddf32818be",
   "metadata": {},
   "source": [
    "## Calculations with the GRAPE algorithm as implemented in QuTiP"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecbd6af6-7be4-41ef-b4e8-8ea688e8a2ae",
   "metadata": {},
   "source": [
    "## (WARNING: TEMPORARILY DISABLED) "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "10f477ef-d262-45e5-8e1e-565285927a7a",
   "metadata": {},
   "source": [
    "We will now approach the same problem, but using the GRAPE algorithm as implemented in QuTiP. This means that we can no longer use the Fourier parametrization of the pulses; instead, the basic assumption is that the pulses are piece-wise constant functions. The control parameters are the values of the function on each of those steps. This is also often called a real-time parametrization of the function.\n",
    "\n",
    "See the QuTiP documentation and tutorials for an explanation of the following steps."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "a8e12009-c245-48e6-96c3-635f72ee69ca",
   "metadata": {},
   "outputs": [],
   "source": [
    "#logger = qt.logging_utils.get_logger()\n",
    "#log_level = qt.logging_utils.INFO"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7c374d80-f514-4f1a-81b2-e69015ae0de7",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Number of time slots; we will use the same number of steps than we did with qocttools.\n",
    "#n_ts = ntsteps -1 # 10\n",
    "## Time allowed for the evolution\n",
    "#evo_time = T # 10"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30a867d3-b69b-4b67-90d9-51e346ddf0a2",
   "metadata": {},
   "outputs": [],
   "source": [
    "## Fidelity error target\n",
    "#fid_err_targ = 1e-10\n",
    "## Maximum iterations for the optisation algorithm\n",
    "#max_iter = 200\n",
    "## Maximum (elapsed) time allowed in seconds\n",
    "#max_wall_time = 120\n",
    "## Minimum gradient (sum of gradients squared)\n",
    "## as this tends to 0 -> local minima has been found\n",
    "#min_grad = 1e-20"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "5af54074-5a6f-471f-b8c7-29d68902d049",
   "metadata": {},
   "outputs": [],
   "source": [
    "## pulse type alternatives: RND|ZERO|LIN|SINE|SQUARE|SAW|TRIANGLE|\n",
    "#p_type = 'SAW'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "aad0420e-9384-4e5a-b461-7864894c1ab1",
   "metadata": {},
   "outputs": [],
   "source": [
    "#result = cpo.optimize_pulse_unitary(H0, V, U0, UCNOT, n_ts, evo_time, \n",
    "#                fid_err_targ=fid_err_targ, min_grad=min_grad, \n",
    "#                max_iter=max_iter, max_wall_time=max_wall_time, \n",
    "#                out_file_ext=None, init_pulse_type=p_type, \n",
    "#                log_level=log_level, gen_stats=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fd639f14-a94a-49f8-90cf-da4249c8448f",
   "metadata": {},
   "outputs": [],
   "source": [
    "#result.stats.report()\n",
    "#print(\"Final evolution\\n{}\\n\".format(result.evo_full_final))\n",
    "#print(\"********* Summary *****************\")\n",
    "#print(\"Final fidelity error {}\".format(result.fid_err))\n",
    "#print(\"Final gradient normal {}\".format(result.grad_norm_final))\n",
    "#print(\"Terminated due to {}\".format(result.termination_reason))\n",
    "#print(\"Number of iterations {}\".format(result.num_iter))\n",
    "#print(\"Completed in {} HH:MM:SS.US\".format(\n",
    "#        datetime.timedelta(seconds=result.wall_time)))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9884e498-29dc-490d-807a-a3c5c44929e2",
   "metadata": {},
   "source": [
    "The calculations are very fast, and the final achieved fidelity is very high. Let us now propagate the evolution operator, both for the initial and for the optimized pulses, to double-check that everything worked, and also to be able to plot later the evolution of the target function in time."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "9be45137-e1a8-4945-8db7-cada9f0b8891",
   "metadata": {},
   "outputs": [],
   "source": [
    "#dt = result.time[1]\n",
    "#UTinitial = qt.identity(4)\n",
    "#UToptimized = qt.identity(4)\n",
    "#Ftinitial = np.zeros(ntsteps)\n",
    "#Ft = np.zeros(ntsteps)\n",
    "#Ftinitial[0] = (1/4**2) * np.abs((UCNOT.dag() * UTinitial).tr())**2\n",
    "#Ft[0] = (1/4**2) * np.abs((UCNOT.dag() * UToptimized).tr())**2\n",
    "#for j in range(n_ts):\n",
    "#    Hoptimized = H0 + result.final_amps[j, 0] * V1 + result.final_amps[j, 1] * V2\n",
    "#    Uoptimizeddt = (-1j * dt * Hoptimized).expm()\n",
    "#    UToptimized = Uoptimizeddt*UToptimized\n",
    "#    Ft[j+1] = (1/4**2) * np.abs((UCNOT.dag() * UToptimized).tr())**2\n",
    "#    Hinitial = H0 + result.initial_amps[j, 0] * V1 + result.initial_amps[j, 1] * V2\n",
    "#    Uinitialdt = (-1j * dt * Hinitial).expm()\n",
    "#    UTinitial = Uinitialdt*UTinitial\n",
    "#    Ftinitial[j+1] = (1/4**2) * np.abs((UCNOT.dag() * UTinitial).tr())**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b1fe18f1-9a3e-4b57-a4d9-56202268a2f3",
   "metadata": {},
   "outputs": [],
   "source": [
    "#plt.rcParams[\"font.size\"] = \"8\"\n",
    "#plt.rcParams[\"figure.autolayout\"] = False\n",
    "\n",
    "#fig, ax = plt.subplots(2, 1, figsize=(5, 5), sharex = True)\n",
    "#fig.subplots_adjust(hspace=0)\n",
    "\n",
    "#ax[0].plot(ts/tau, Ftinitial, label = r\"$F(U_{\\rm initial}(t))$\")\n",
    "#ax[0].plot(ts/tau, Ft, label = r\"$F(U(t))$\")\n",
    "##ax[0].set_xlabel(r\"$t/\\tau$\")\n",
    "#ax[0].set_xlim(0.0, T/tau)\n",
    "#ax[0].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "#ts = np.linspace(0, T, ntsteps)\n",
    "\n",
    "#ax[1].step(result.time/tau,\n",
    "#           np.hstack((result.initial_amps[:, 0], result.initial_amps[-1, 0])),\n",
    "#           where='post', label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "#ax[1].step(result.time/tau,\n",
    "#           np.hstack((result.initial_amps[:, 1], result.initial_amps[-1, 1])),\n",
    "#           where='post', label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "#ax[1].step(result.time/tau,\n",
    "#           np.hstack((result.final_amps[:, 0], result.final_amps[-1, 0])),\n",
    "#           where='post', label = r\"$g_x^{\\rm opt}(t)$\")\n",
    "#ax[1].step(result.time/tau,\n",
    "#           np.hstack((result.final_amps[:, 1], result.final_amps[-1, 1])),\n",
    "#           where='post', label = r\"$g_y^{\\rm opt}(t)$\")\n",
    "\n",
    "#ax[1].set_xlabel(r\"$t/\\tau$\")\n",
    "#ax[1].set_xlim(0.0, T/tau)\n",
    "##ax[1].label_outer()\n",
    "#ax[1].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "#plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "43164fc8-a30a-4275-a1b5-4216c6d2dd0e",
   "metadata": {},
   "outputs": [],
   "source": [
    "# This file is used by the testing script of the code.\n",
    "with open(\"data\", \"w\") as f:\n",
    "    for i in data:\n",
    "        f.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimization on a closed system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial demonstrates a basic optimization on a simple three-level quantum model of the Nitrogen-vancancy center in diamond. The goal is to find a pulse that drives the system from its ground to its highest energy state."
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is defined by the three-level Hamiltonian:\n",
    "\n",
    "\\begin{align}\n",
    "H(u, t) &= H_0 + V(u, t),\n",
    "\\\\\n",
    "H_0 &= -B_s S_z + N_z S_z^2 + N_{xy}(S_x^2-S_y^2),\n",
    "\\\\\n",
    "V(u, t) &= - g_x(u, t) B_d S_x - g_y(u, t) B_d S_y.\n",
    "\\end{align}\n",
    "\n",
    "This model is taken from [Ikeda *et al*, Science Advances 6, eabb4019 (2020)]. $S_x, S_y$ and $S_z$ are the spin operators, whereas $N_z$, $N_{xy}$, $B_s$, and $B_d$ are constants. The shape of the real time-dependent control functions $g_x$ and $g_y$ is explained below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib\n",
    "from copy import deepcopy\n",
    "import matplotlib.pyplot as plt\n",
    "import qutip as qt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.target as target\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.solvers as solvers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is good practice to print the precise version of the software that you are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we build the static Hamiltonian $H_0$ (stored into the Qobj object `H0`), and the two coupling operators $V_1 = -B_d S_x$ and $V_2 = -B_d S_y$ (stored into the Qobj objects `V[0]` and `V[1]`). The function `system_definition` in principle returns some Lindblad operators, but those are not used in this tutorial. The field-free eigenvalues are stored stored in the array `e` and the eigenfunctions in `psi`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Sx = qt.jmat(1, \"x\")\n",
    "Sy = qt.jmat(1, \"y\")\n",
    "Sz = qt.jmat(1, \"z\")\n",
    "Bs = 0.3\n",
    "Nz = 1.00\n",
    "Nxy = 0.05\n",
    "Bd = 0.1\n",
    "omega = 1.00\n",
    "# We assume in this tutorial that the dissipation is zero.\n",
    "# gamma = 0.2\n",
    "gamma = 0.0\n",
    "beta = 3.0\n",
    "d = 3\n",
    "dim = d**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def system_definition():\n",
    "    H0 = -Bs * Sz + Nz * Sz**2 + Nxy * (Sx**2 - Sy**2)\n",
    "    Vx = -Bd * Sx\n",
    "    Vy = -Bd * Sy\n",
    "    A = []\n",
    "    e, psi = H0.eigenstates()\n",
    "    for i in range(d):\n",
    "        for j in range(d):\n",
    "            if j == i:\n",
    "                continue\n",
    "            gammaij = gamma * np.exp(-beta*e[j]) / (np.exp(-beta*e[i])+np.exp(-beta*e[j]))\n",
    "            A.append( np.sqrt(gammaij) * psi[j] * psi[i].dag())\n",
    "    return H0, [Vx, Vy], A, e, psi\n",
    "\n",
    "H0, V, A, e, psi = system_definition()\n",
    "\n",
    "print(\"Field-free eigenvalues = {}\".format(e))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to create the an object of [hamiltonian](../modules.rst#hamiltonians.hamiltonian) class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The objective is for the system to transition from the ground state to the first excited state. Therfore, we will define a *relevant* frequency `omega`, given by the energy difference between those states. The relevant period is the one corresponding to that frequency"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = e[1]-e[0]\n",
    "tau = 2.0*np.pi/omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we consider the total propagation time. To make the transition easier, it is useful to make sure that the total propagation time includes several time the relevant period `tau`. In this case, we will make it ten times larger. \n",
    "\n",
    "The definition of a propagation time $T$ also defines a \"fundamental frequency\", $\\omega_0 = 2\\pi/T$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 3 * tau\n",
    "omega0 = 2.0*np.pi/T\n",
    "print(\"omega0 = {:.4f}\".format(omega0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we create the [Target](../modules.rst#target.Target) object. In this case, it is an \"expectationvalue\" type, since what we want to do is to maximize the expectation value of $O = \\vert\\phi_1\\rangle \\langle\\phi_1\\vert$, i.e. the population of the first excited state. See the documenation of the [Target](../modules.rst#target.Target) class to learn about other options that can be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "O = psi[1] * psi[1].dag()\n",
    "tg = target.Target(\"expectationvalue\", operator = O)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, we are defining the target function as:\n",
    "\\begin{equation}\n",
    "F(\\psi, u) = \\langle \\psi \\vert O \\vert \\psi \\rangle\\,,\n",
    "\\end{equation}\n",
    "so that the function of the control parameters $u$ that is to be optimized is:\n",
    "\\begin{equation}\n",
    "G(u) = F(\\psi_u(T), u)\\,.\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we must create the [pulse](../modules.rst#pulses.pulse) objects, i.e. the control functions. In this case, we have two perturbation operators, and we need two control functions. In this example, we will choose a Fourier-series parametrization for both control functions.\n",
    "\n",
    "A Fourier series must be cutoff at some maximum frequency $M\\omega_0$. We will set $M=15$, which ensures that the relevant frequency $\\omega$ is lower than the maximum frequency. Then we must give initial-guess values to the amplitudes of the Fourier series (which are the control parameters). There are $2M+1$ parameteres in each Fourier series ($M$ for the cosines, $M$ for the sines, and 1 for the zero-frequency term). We will simply set some of the parameters to some $\\kappa$ value, and zero for the rest of them. \n",
    "\n",
    "A better alternative would be to use random values, but in order to make sure that the tutorial always produces the same result, we will not use that option here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 10\n",
    "kappa = 1.0\n",
    "\n",
    "u1 = np.zeros(2*M+1)\n",
    "u2 = np.zeros(2*M+1)\n",
    "u1[3] = kappa\n",
    "u1[5] = kappa\n",
    "u2[5] = kappa\n",
    "u2[4] = kappa\n",
    "\n",
    "\n",
    "# The following code sets random numbers for the control parameters\n",
    "#bound = kappa\n",
    "#a = -bound\n",
    "#b = bound\n",
    "#u1 = (b-a) * np.random.rand(2*M+1) + a\n",
    "#u2 = (b-a) * np.random.rand(2*M+1) + a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We place the pulse objects into a list `f`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = []\n",
    "f.append(pulses.pulse(\"fourier\", T, u = u1))\n",
    "f.append(pulses.pulse(\"fourier\", T, u = u2))\n",
    "f0 = deepcopy(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`u0` will hold all the control parameters (the parameters of all the pulses together)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u0 = pulses.pulse_collection_get_parameters(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot to see how the pulses look like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "ts = np.linspace(0, T, 200)\n",
    "ax.plot(ts/T, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax.plot(ts/T, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax.set_xlabel(\"t/T\")\n",
    "ax.set_xlim(0, 1)\n",
    "ax.legend()\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now build the main object, of class [Qoct](../modules.rst#qoct.Qoct). Along with the Hamiltonian `H`, the target `tg`, and the set of control functions `f`, we need to pass the initial state, in our case the ground states `psi[0]`. Also, `ntsteps`, which is the number of time steps used to discretize the time interval for the numerical integrations of the equations that need to be solved. The higher, the more precise the calculations will be, but they will also be slower."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ntsteps = 400\n",
    "opt = qoct.Qoct(H, T, ntsteps, tg, f, psi[0])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us see what the initial pulse does: we will propagate the system using the initial guess pulses, and we will plot the populations of each state as a function of time.\n",
    "\n",
    "In order to the system propagation, we will use the [solve()](../modules.rst#solvers.solve) function and, in this case, the `cfmagnus4` method (see the documentation of the [solve()](../modules.rst#solvers.solve) function to learn about the propagation methods used by qocttools)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, psi[0], ts)\n",
    "ps = np.zeros([ntsteps, d])\n",
    "for k in range(d):\n",
    "    for j in range(ntsteps):\n",
    "        ps[j, k] = qt.expect(psi[k]*psi[k].dag(), res[j])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts, ps[:, 0], label = r\"$P_0$\")\n",
    "ax.plot(ts, ps[:, 1], label = r\"$P_1$\")\n",
    "ax.plot(ts, ps[:, 2], label = r\"$P_2$\")\n",
    "ax.set_xlabel(\"t\")\n",
    "ax.set_xlim(0, T)\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As one can see, it does nothing: the populations of the excited state remain close to zero, and the population of the initial ground state remains close to one. This can be checked by computing the value of the target function for this initial guess pulse, which can be done simply by using the `gfunc` method:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"G(u0) = {}\".format(opt.gfunc(u0)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We want to change that, and force a population transfer to the first excited state."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before launching the optimization, let us check that the gradient is computed correctly. We can use for that purpose the [check_grad()](../modules.rst#qoct.Qoct.check_grad) method of the [Qoct](../modules.rst#qoct.Qoct) class. It computes the gradient using the QOCT formula, and using finite differences. Both numbers should match if everything is OK."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u0)\n",
    "print(derqoct, dernum, error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we launch the maximization calculation, using the [maximize()](../modules.rst#qoct.Qoct.maximize) method. Here, we will set a maximum number of evaluations through the `maxeval` argument, a *stop value* to stop the iterations in case a certain threshold is reached through the `stopval`argument, and we will set `verbose` to True so that we can see the optimization process as it runs. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(f, u0)\n",
    "x, optval, result = opt.maximize(maxeval = 100,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True,\n",
    "                                 upper_bounds = 2*kappa * np.ones_like(u0),\n",
    "                                 lower_bounds = -2*kappa * np.ones_like(u0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the optimization proceeds pretty fast towards the optimal value, achieving full population of the target excited state. Let us see this transfer in a plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, psi[0], ts)\n",
    "ps = np.zeros([ntsteps, d])\n",
    "for k in range(d):\n",
    "    for j in range(ntsteps):\n",
    "        ps[j, k] = qt.expect(psi[k]*psi[k].dag(), res[j])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now plot the results: in the top plot, the populations of the states as they evolve in time; in the bottom plot, the initial and optimized pulses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"font.size\"] = \"8\"\n",
    "plt.rcParams[\"figure.autolayout\"] = False\n",
    "\n",
    "fig, ax = plt.subplots(2, 1, figsize=(3, 3), sharex = True)\n",
    "fig.subplots_adjust(hspace=0)\n",
    "\n",
    "ax[0].plot(ts/tau, ps[:, 0], label = r\"$P_0$\")\n",
    "ax[0].plot(ts/tau, ps[:, 1], label = r\"$P_1$\")\n",
    "ax[0].plot(ts/tau, ps[:, 2], label = r\"$P_2$\")\n",
    "#ax.set_xlabel(r\"$t/\\tau$\")\n",
    "ax[0].set_xlim(0, T/tau)\n",
    "ax[0].set_ylim(0, 1)\n",
    "ax[0].set_yticks([0.2, 0.4, 0.6, 0.8, 1.0])\n",
    "ax[0].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "ts = np.linspace(0, T, 200)\n",
    "\n",
    "ax[1].plot(ts/tau, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f[0].fu(ts), label = r\"$g_x^{\\rm opt}(t)$\")\n",
    "ax[1].plot(ts/tau, f[1].fu(ts), label = r\"$g_y^{\\rm opt}(t)$\")\n",
    "ax[1].set_xlabel(r\"$t/\\tau$\")\n",
    "ax[1].set_xlim(0, T/tau)\n",
    "ax[1].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "fig.savefig(\"closed.pdf\", bbox_inches = 'tight')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculations using the \"generic\" target type"
   ]
  },
  {
   "attachments": {},
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, let us repeat the very same calculations, but using the \"generic\" option for defining the target objetc. In this way, one can define any target function in the form:\n",
    "\\begin{equation}\n",
    "F = F(\\psi, u)\\,,\n",
    "\\end{equation}\n",
    "where $\\psi$ is the wave function at the end of the propagation (since, for this kind of calculations, qocttools only performs terminal-like optimal control in the current version). In addition, $F$ may depend on the set of parameters $u$ of the control functions (in order to allow for the presence of penalties, etc. Often, this explicit dependency is absent, but the function interface must allow it).\n",
    "\n",
    "In addition, one must define the derivative of this function, $\\frac{\\delta F}{\\delta \\psi^*}$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Thus, for example, for the simple case that we are dealing with:\n",
    "\\begin{equation}\n",
    "F(\\psi, u) = \\langle \\psi \\vert O \\vert \\psi \\rangle\\,,\n",
    "\\end{equation}\n",
    "and\n",
    "\\begin{equation}\n",
    "\\frac{\\delta F}{\\delta \\psi^*} = O \\vert \\psi\\rangle\\,.\n",
    "\\end{equation}\n",
    "These functions can be coded as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Fpsi(psi, u):\n",
    "    return qt.expect(O, psi)\n",
    "def dFdpsi(psi, u):\n",
    "    return O * psi"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If there were an explicit dependence on $u$, we would also have to define a function $\\frac{\\partial F}{\\partial u}$, to be passed to the `dFdu` argument of the `Target` class constructor. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, we can define the [Target](../modules.rst#target.Target) object as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target(\"generic\", Fyu = Fpsi, dFdy = dFdpsi)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, ntsteps, tg, f, psi[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(f, u0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u0)\n",
    "print(derqoct, dernum, error)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(f, u0)\n",
    "x, optval, result = opt.maximize(maxeval = 100,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True,\n",
    "                                 upper_bounds = 2*kappa * np.ones_like(u0),\n",
    "                                 lower_bounds = -2*kappa * np.ones_like(u0))\n",
    "data.append(optval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Of course, it does not make sense to use this \"generic\" mode to do the same calculation than we did before. However, it can be used to define any target function"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Calculations using automatic differentiation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(WARNING: THE SUPPORT FOR AUTOMATIC DIFFERENTIATION IS STILL IN DEVELOPMENT, AND THEREFORE IT IS PROBABLY VERY UNSTABLE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In some circumstances, one may want to use a target definition whose derivative with respect to the wave function (or the density matrix, or the evolution operator) is not easy to write as a Python function, or is not known, etc. In that case, one may use automatic differentiation and skip the specification of the `dFdy` function. For that purpose, qocttools is using the jax library, that should be loaded with:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import jax; jax.config.update('jax_platform_name', 'cpu')\n",
    "import jax.numpy as jnp"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(The specification of cpu instead of gpu can probably be lifted if you can use GPUs, but I have not tested that)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We do have to define the function $F$ to pass it to the `Fyu` argument of the Target object constructor. The key point is that this function no longer works with qutip objects, but it should work with jax.numpy objects, and do the calculations using those. Thus, for example, to replicate the calculations above:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def Fpsi(psi, u):\n",
    "    return jnp.vdot(psi, jnp.matmul(O.full(), psi)).real"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Then, when building the Target object, the `dFdy` argument should be given the string 'ad' as value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target(\"generic\", Fyu = Fpsi, dFdy = 'ad')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The rest is the same:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, ntsteps, tg, f, psi[0])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(f, u0)\n",
    "x_ad, optval, result = opt.maximize(maxeval = 100,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True,\n",
    "                                 upper_bounds = 2*kappa * np.ones_like(u0),\n",
    "                                 lower_bounds = -2*kappa * np.ones_like(u0))\n",
    "data.append(optval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this case, the calculations are different; the slight numerical differences that one gets when computing the derivative with AD with respect to using the analytical formula seem to lead to a different convergence history. But the final optimal solution is very similar, as it can be checked computing the norm of the difference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(\"||x|| = {}, ||x_ad|| = {}\".format(np.linalg.norm(x), np.linalg.norm(x_ad)))\n",
    "print(\"||x-x_ad|| / ||x|| = {}\".format(np.linalg.norm(x-x_ad)/np.linalg.norm(x)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It should be stressed that automatic differentiation is used to obtain the derivative\n",
    "\\begin{equation}\n",
    "\\frac{\\delta F}{\\delta \\psi^*}\n",
    "\\end{equation}\n",
    "that is used to build the finite-value condition for the propagation of the costate in the computation of the optimal control gradient.\n",
    "\n",
    "It is **not** used to compute this gradient itself, i.e. the derivatives:\n",
    "\\begin{equation}\n",
    "\\frac{\\delta G}{\\delta u_m}\n",
    "\\end{equation}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Krotov's algorithm"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we will solve the same problem, but using Krotov's algorithm. This is however not exactly the same problem, as Krotov's algorithm requires the representation of the function in real time, i.e. the control parameters are the values of the function at each point in the time grid. qocttools includes a basic implementation of this algorithm, that we will use here."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "First, let us restore the pulses with the original initial guess values."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(f, u0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we must use pulse objects with the \"realtime\" representation. In the following, we define such pulses and place them into the list ``f``. The initial values are taken by dumping the values of the Fourier pulses that were defined above."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ntsteps = 400\n",
    "ts = np.linspace(0, T, ntsteps)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ft = []\n",
    "ft.append( pulses.pulse(\"realtime\", T, u = f[0].fu(ts)) )\n",
    "ft.append( pulses.pulse(\"realtime\", T, u = f[1].fu(ts)) )"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`u0` will now hold all the \"realtime\" control parameters (the parameters of all the pulses together)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u0 = pulses.pulse_collection_get_parameters(ft)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Definition of the target; once again, we want to maximize an expectation value. However, note that Krotov's algorithm **requires** of the presence of a penalty term, which in this case reads:\n",
    "\\begin{equation}\n",
    "-\\alpha\\int_0^T\\!{\\rm d}t g^2_x(t) - \\alpha\\int_0^T\\!{\\rm d}t g^2_y(t)\\,.\n",
    "\\end{equation}\n",
    "since we have two control functions in this problem. The value of $\\alpha$ must be chosen by the user."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target('expectationvalue',\n",
    "                   operator = O, alpha = 0.005)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The definition of the [Qoct](../modules.rst#qoct.Qoct) object must include, for Krotov's calculation, the specification of ``solve_method = 'rk4'``, as it is the only propagation scheme currently supported by the Krotov qocttools module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "opt = qoct.Qoct(H, T, ntsteps, tg, ft, psi[0],\n",
    "                solve_method = 'rk4')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can launch the optimization. Note that ``algorithm = -1`` means Krotov's algorithm."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(ft, u0)\n",
    "x, optval, result = opt.maximize(algorithm = -1,\n",
    "                                 maxeval = 10,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True)\n",
    "data.append(optval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Notice the very fast convergence; this is one of the advantages of Krotov's algorithm. The speed of convergence, and the quality of the maximum, will however depend on the value of $\\alpha$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now plot the results: in the top plot, the populations of the states as they evolve in time; in the bottom plot, the initial and optimized pulses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pulses.pulse_collection_set_parameters(ft, x)\n",
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('rk4', H, ft, psi[0], ts)\n",
    "ps = np.zeros([ntsteps, d])\n",
    "for k in range(d):\n",
    "    for j in range(ntsteps):\n",
    "        ps[j, k] = qt.expect(psi[k]*psi[k].dag(), res[j])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"font.size\"] = \"8\"\n",
    "plt.rcParams[\"figure.autolayout\"] = False\n",
    "\n",
    "fig, ax = plt.subplots(2, 1, figsize=(3, 3), sharex = True)\n",
    "fig.subplots_adjust(hspace=0)\n",
    "\n",
    "ax[0].plot(ts/tau, ps[:, 0], label = r\"$P_0$\")\n",
    "ax[0].plot(ts/tau, ps[:, 1], label = r\"$P_1$\")\n",
    "ax[0].plot(ts/tau, ps[:, 2], label = r\"$P_2$\")\n",
    "#ax.set_xlabel(r\"$t/\\tau$\")\n",
    "ax[0].set_xlim(0, T/tau)\n",
    "ax[0].set_ylim(0, 1)\n",
    "ax[0].set_yticks([0.2, 0.4, 0.6, 0.8, 1.0])\n",
    "ax[0].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "ts = np.linspace(0, T, 400)\n",
    "\n",
    "ax[1].plot(ts/tau, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, ft[0].fu(ts), label = r\"$g_x^{\\rm opt}(t)$\")\n",
    "ax[1].plot(ts/tau, ft[1].fu(ts), label = r\"$g_y^{\\rm opt}(t)$\")\n",
    "ax[1].set_xlabel(r\"$t/\\tau$\")\n",
    "ax[1].set_xlim(0, T/tau)\n",
    "ax[1].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This file is used by the testing script of the code.\n",
    "with open(\"data\", \"w\") as f:\n",
    "    for i in data:\n",
    "        f.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

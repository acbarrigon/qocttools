{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimization on a closed system: creation of a target evolution operator"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial demonstrates a basic optimization performed on a simple three-level quantum model of the Nitrogen-vancancy center in diamond. The goal, in this case, is to find a pulse that induces a given evolution operator (a *gate*, in the language of quantum information theory)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is defined by the three-level Hamiltonian:\n",
    "\\begin{align}\n",
    "H(u, t) &= H_0 + V(u, t),\n",
    "\\\\\n",
    "H_0 &= -B_s S_z + N_z S_z^2 + N_{xy}(S_x^2-S_y^2),\n",
    "\\\\\n",
    "V(u, t) &= - g_x(u, t) B_d S_x - g_y(u, t) B_d S_y.\n",
    "\\end{align}\n",
    "This model is taken from [Ikeda *et al*, Science Advances 6, eabb4019 (2020)]. $S_x, S_y$ and $S_z$ are the spin operators, whereas $N_z$, $N_{xy}$, $B_s$, and $B_d$ are constants. The shape of the real time-dependent control functions $g_x$ and $g_y$ is explained below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib\n",
    "from copy import deepcopy\n",
    "import matplotlib.pyplot as plt\n",
    "import qutip as qt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.target as target\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.solvers as solvers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is good practice to print the precise version of the software that you are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "data = []"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we build the static Hamiltonian $H_0$ (stored into the Qobj object `H0`), and the two coupling operators $V_1 = -B_d S_x$ and $V_2 = -B_d S_y$ (stored into the Qobj objects `V[0]` and `V[1]`). The function `system_definition` in principle returns some Lindblad operators, but those are not used in this tutorial. The field-free eigenvalues are stored stored in the array `e` and the eigenfunctions in `psi`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Sx = qt.jmat(1, \"x\")\n",
    "Sy = qt.jmat(1, \"y\")\n",
    "Sz = qt.jmat(1, \"z\")\n",
    "Bs = 0.3\n",
    "Nz = 1.00\n",
    "Nxy = 0.05\n",
    "Bd = 0.1\n",
    "omega = 1.00\n",
    "# We assume in this tutorial that the dissipation is zero.\n",
    "# gamma = 0.2\n",
    "gamma = 0.0\n",
    "beta = 3.0\n",
    "d = 3\n",
    "dim = d**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def system_definition():\n",
    "    H0 = -Bs * Sz + Nz * Sz**2 + Nxy * (Sx**2 - Sy**2)\n",
    "    Vx = -Bd * Sx\n",
    "    Vy = -Bd * Sy\n",
    "    A = []\n",
    "    e, psi = H0.eigenstates()\n",
    "    for i in range(d):\n",
    "        for j in range(d):\n",
    "            if j == i:\n",
    "                continue\n",
    "            gammaij = gamma * np.exp(-beta*e[j]) / (np.exp(-beta*e[i])+np.exp(-beta*e[j]))\n",
    "            A.append( np.sqrt(gammaij) * psi[j] * psi[i].dag())\n",
    "    return H0, [Vx, Vy], A, e, psi\n",
    "\n",
    "H0, V, A, e, psi = system_definition()\n",
    "\n",
    "print(\"Field-free eigenvalues = {}\".format(e))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to create the an object of [hamiltonian](../modules.rst#hamiltonians.hamiltonian) class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, V)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The energy and time scales of the system are determined by the *natural* frequencies, which are the eigenenergy differences. We will use the difference between the ground and the first excited state to define a *characteristic* system frequency `omega`, that we will use as a reference for the rest of the definitions. `tau` will be the corresponding period."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = e[1]-e[0]\n",
    "tau = 2.0*np.pi/omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we consider the total propagation time. To make the process easier, it is useful to make sure that the total propagation time includes several times the characteristic period `tau`. In this case, we will make it ten times larger. This also defines a *fundamental frequency*, $\\omega_0 = 2\\pi/T$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 5 * tau\n",
    "omega0 = 2.0*np.pi/T\n",
    "print(\"omega0 = {:.4f}\".format(omega0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we create the [Target](../modules.rst#target.Target) object. In this case, it is an \"evolutionoperator\" type, since what we want to do is to find a pulse that induces a given evolution operator. For this example, we choose a three-level \"gate\" that is a rotation gate (a member of $SU(3)$), built by taking ane exponential of the first Gell-Mann matrix (The Gell-Mann matrices are a set of eight linearly independent 3x3 traceless Hermitian matrices. They span the Lie algebra of the SU(3) group in the defining representation. They are the equivalent of the Pauli matrices, for three-dimensional problems)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "lambda1 = qt.Qobj( np.array([[0, 1, 0], [1, 0, 0], [0, 0, 0]]) )\n",
    "R = ((-1j * (np.pi/2) ) * lambda1).expm()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "tg = target.Target(\"evolutionoperator\", Utarget = R)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we must create the [pulse](../modules.rst#pulses.pulse) objects, i.e. the control functions. In this case, we have two perturbation operators, and we need two control functions. In this example, we will choose a Fourier-series parametrization for both control functions.\n",
    "\n",
    "A Fourier series must be cutoff at some maximum frequency $M\\omega_0$. We will set $M=15$, which ensures that the relevant frequency $\\omega$ is lower than the maximum frequency. Then we must give initial-guess values to the amplitudes of the Fourier series (which are the control parameters). There are $2M+1$ parameteres in each Fourier series ($M$ for the cosines, $M$ for the sines, and 1 for the zero-frequency term). We will simply set some of the parameters to some $\\kappa$ value, and zero for the rest of them. \n",
    "\n",
    "A better alternative would be to use random values, but in order to make sure that the tutorial always produces the same result, we will not use that option here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 15\n",
    "kappa = 0.1\n",
    "\n",
    "u1 = np.zeros(2*M+1)\n",
    "u2 = np.zeros(2*M+1)\n",
    "u1[3] = kappa\n",
    "u2[4] = kappa\n",
    "\n",
    "# The following code sets random numbers for the control parameters\n",
    "#a = -bound\n",
    "#b = bound\n",
    "#u1 = (b-a) * np.random.rand(2*M+1) + a\n",
    "#u2 = (b-a) * np.random.rand(2*M+1) + a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We place the pulse objects into a list `f`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = []\n",
    "f.append(pulses.pulse(\"fourier\", T, u = u1))\n",
    "f.append(pulses.pulse(\"fourier\", T, u = u2))\n",
    "f0 = deepcopy(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`u0` will hold all the control parameters (the parameters of all the pulses together)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u0 = pulses.pulse_collection_get_parameters(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot to see how the pulses look like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "ts = np.linspace(0, T, 200)\n",
    "ax.plot(ts/tau, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax.plot(ts/tau, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax.set_xlabel(r\"$t/\\tau$\")\n",
    "ax.set_xlim(0, T/tau)\n",
    "ax.legend()\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now build the main object, of class [Qoct](../modules.rst#qoct.Qoct). Along with the Hamiltonian `H`, the target `tg`, and the set of control functions `f`, we need to pass the initial state, which in this case it has to be the identity matrix (the evolution operator at time zero). Also, `ntsteps`, which is the number of time steps used to discretize the time interval for the numerical integration. The higher, the more precise the calculations will be, but they will also be slower."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ntsteps = 200\n",
    "U0 = qt.qeye(d)\n",
    "opt = qoct.Qoct(H, T, ntsteps, tg, f, U0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us see what the initial pulse does: we will propagate the system using the initial guess pulses, and we will plot the value of the target function as it evolves in time:\n",
    "\n",
    "\\begin{equation}\n",
    "F(t) = \\frac{1}{d^2} \\vert R \\cdot U(t)\\vert^2\n",
    "\\end{equation}\n",
    "\n",
    "where the dot product is the Fröbenius inner product.\n",
    "\n",
    "In order to the system propagation, we will use the [solve()](../modules.rst#solvers.solve) function and, in this case, the `cfmagnus4` method (see the documentation of the [solve()](../modules.rst#solvers.solve) function to learn about the propagation methods used by qocttools)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, U0, ts)\n",
    "Ft = np.zeros(ntsteps)\n",
    "for j in range(ntsteps):\n",
    "    Ft[j] = (1/d**2) * np.abs((R.dag() * res[j]).tr())**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts/tau, Ft, label = r\"$F(U(t))$\")\n",
    "ax.set_xlabel(r\"$t/tau$\")\n",
    "ax.set_xlim(0, T/tau)\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As one can see, the value of the plotted function remains close to 0.1 all the time when using the initial guess (the value of the function is one when the evolution operator is equal -- or equivalent -- to the target operator $R$)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before launching the optimization, let us check that the gradient is computed correctly. We can use for that purpose the [check_grad()](../modules.rst#qoct.Qoct.check_grad) method of the [Qoct](../modules.rst#qoct.Qoct) class. It computes the gradient using the QOCT formula, and using finite differences. Both numbers should match if everything is OK."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "derqoct, dernum, error, elapsed_time = opt.check_grad(u0)\n",
    "print(derqoct, dernum, error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we launch the maximization calculation, using the [maximize()](../modules.rst#qoct.Qoct.maximize) method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, optval, result = opt.maximize(maxeval = 100,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True)\n",
    "data.append(optval)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, the optimization proceeds pretty fast towards the optimal value. Let us see the behaiour of the target function in a plot."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, U0, ts)\n",
    "Ft = np.zeros(ntsteps)\n",
    "for j in range(ntsteps):\n",
    "    Ft[j] = (1/d**2) * np.abs((R.dag() * res[j]).tr())**2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we can look at how the target function evolves in time until it reaches a value close to the ideal maximum on one, and we compare now in a plot the initial guess versus the optimized pulse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "plt.rcParams[\"font.size\"] = \"8\"\n",
    "plt.rcParams[\"figure.autolayout\"] = False\n",
    "\n",
    "fig, ax = plt.subplots(2, 1, figsize=(3, 3), sharex = True)\n",
    "fig.subplots_adjust(hspace=0)\n",
    "\n",
    "ax[0].plot(ts/tau, Ft, label = r\"$F(U(t))$\")\n",
    "#ax[0].set_xlabel(r\"$t/\\tau$\")\n",
    "ax[0].set_xlim(0, T/tau)\n",
    "#ax[0].label_outer()\n",
    "ax[0].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "#fig, ax = plt.subplots(2, figsize=(3,2))\n",
    "\n",
    "ts = np.linspace(0, T, 200)\n",
    "\n",
    "ax[1].plot(ts/tau, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax[1].plot(ts/tau, f[0].fu(ts), label = r\"$g_x^{\\rm opt}(t)$\")\n",
    "ax[1].plot(ts/tau, f[1].fu(ts), label = r\"$g_y^{\\rm opt}(t)$\")\n",
    "ax[1].set_xlabel(r\"$t/\\tau$\")\n",
    "ax[1].set_xlim(0, T/tau)\n",
    "#ax[1].label_outer()\n",
    "ax[1].legend(bbox_to_anchor = (1.0, 1.0), loc = 'upper left')\n",
    "\n",
    "fig.savefig(\"closed-gate.pdf\", bbox_inches = 'tight')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# This file is used by the testing script of the code.\n",
    "with open(\"data\", \"w\") as f:\n",
    "    for i in data:\n",
    "        f.write(\"{:.14e}\\n\".format(i))"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Optimization for an open system"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This tutorial demonstrates a basic optimization of a simple three-level quantum model of the Nitrogen-vancancy center in diamond. It is treated as an open system (using the time-dependent Lindblad equation), subject to some dissipative terms.\n",
    "\n",
    "The goal is to find a pulse that attempts to *maintain* the system in the first excited state, as much as possible, fighting the dissipation induced the environment that would otherwise drive the system towards the thermal state. In other words, we start the simulation from the first excited state, and try to find that pulse which, at the end of the simulation time $T$, produces the highest possible population of that same state."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The model is defined by the three-level Hamiltonian:\n",
    "\n",
    "\\begin{eqnarray}\n",
    "H(u, t) &=& H_0 + V(u, t),\n",
    "\\\\\n",
    "H_0 &=& -B_s S_z + N_z S_z^2 + N_{xy}(S_x^2-S_y^2),\n",
    "\\\\\n",
    "\\label{eq:tdpart}\n",
    "V(u, t) &=& - g_x(u, t) B_d S_x - g_y(u, t) B_d S_y.\n",
    "\\end{eqnarray}\n",
    "\n",
    "This model is taken from [Ikeda *et al*, Science Advances 6, eabb4019 (2020)]. $S_x, S_y$ and $S_z$ are the spin operators, whereas $N_z$, $N_{xy}$, $B_s$, and $B_d$ are constants. The shape of the real time-dependent control functions $g_x$ and $g_y$ is explained below.\n",
    "\n",
    "The system is governed by Lindblad's equation:\n",
    "\n",
    "\\begin{align}\n",
    "\\dot{\\rho}(t) = -i\\left[H(u, t), \\rho(t)\\right] \n",
    "+ \\sum_{ij} \\gamma_{ij} \\left( V_{ij}\\rho(t)V^\\dagger_{ij} - \\frac{1}{2}\n",
    "\\lbrace\n",
    "V_{ij}^\\dagger V_{ij}, \\rho(t)\n",
    "\\rbrace\\right)\\,.\n",
    "\\label{eq:lindblad0-eq}\n",
    "\\end{align}\n",
    "\n",
    "The transition operators are $V_{ij} = \\vert E_i\\rangle\\langle E_j\\vert$, and the dissipative constants are $\\gamma_{ij} = \\gamma e^{-\\beta E_i} / (e^{-\\beta E_i}+e^{-\\beta E_j})$ and $\\gamma_{ii}=0$, where $\\beta = 1/(k_{\\rm B}T)$ is the inverse of the temperature, and $\\gamma$ is a rate constant. Notice that this dissipation model ensures the detailed balance condition, $\\gamma_{ij}e^{-\\beta E_j} = \\gamma_{ji}e^{-\\beta E_i}$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "import matplotlib\n",
    "from copy import deepcopy\n",
    "import matplotlib.pyplot as plt\n",
    "import qutip as qt\n",
    "import nlopt"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import qocttools\n",
    "import qocttools.hamiltonians as hamiltonians\n",
    "import qocttools.target as target\n",
    "import qocttools.pulses as pulses\n",
    "import qocttools.qoct as qoct\n",
    "import qocttools.solvers as solvers"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is good practice to print the precise version of the software that you are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "qocttools.about()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we build the static Hamiltonian $H_0$ (stored into the Qobj object `H0`), and the two coupling operators $V_1 = -B_d S_x$ and $V_2 = -B_d S_y$ (stored into the Qobj objects `V[0]` and `V[1]`). The function `system_definition` also returns some Lindblad operators into the `A` list, as defined above. The field-free eigenvalues are stored stored in the array `e` and the eigenfunctions in `psi`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "Sx = qt.jmat(1, \"x\")\n",
    "Sy = qt.jmat(1, \"y\")\n",
    "Sz = qt.jmat(1, \"z\")\n",
    "Bs = 0.3\n",
    "Nz = 1.00\n",
    "Nxy = 0.05\n",
    "Bd = 0.1\n",
    "omega = 1.00\n",
    "gamma = 0.01\n",
    "beta = 3.0\n",
    "d = 3\n",
    "dim = d**2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def system_definition():\n",
    "    H0 = -Bs * Sz + Nz * Sz**2 + Nxy * (Sx**2 - Sy**2)\n",
    "    Vx = -Bd * Sx\n",
    "    Vy = -Bd * Sy\n",
    "    A = []\n",
    "    e, psi = H0.eigenstates()\n",
    "    for i in range(d):\n",
    "        for j in range(d):\n",
    "            if j == i:\n",
    "                continue\n",
    "            gammaij = gamma * np.exp(-beta*e[j]) / (np.exp(-beta*e[i])+np.exp(-beta*e[j]))\n",
    "            A.append( np.sqrt(gammaij) * psi[j] * psi[i].dag())\n",
    "    return H0, [Vx, Vy], A, e, psi\n",
    "\n",
    "H0, V, A, e, psi = system_definition()\n",
    "\n",
    "print(\"Field-free eigenvalues = {}\".format(e))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first step is to create the an object of [hamiltonian](../modules.rst#hamiltonians.hamiltonian) class:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "H = hamiltonians.hamiltonian(H0, V, A)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For later use, let us compute the equilibrium thermal state, $\\rho_\\beta = \\frac{e^{-\\beta H_0}}{{\\rm Tr}e^{-\\beta H_0}}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rhothermal = (-beta * H0).expm()\n",
    "rhothermal = rhothermal / rhothermal.tr()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us define a *relevant* frequency, characteristic of the system, that gives us an idea of the energies and time scales. `omega` will be given by the energy difference between the ground and first excited state. Correspondingly, we can define a *relevant* period, corresponding to that frequency"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "omega = e[1]-e[0]\n",
    "tau = 2.0*np.pi/omega"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we consider the total propagation time. To make the transition easier, it is useful to make sure that the total propagation time includes several time the relevant period `tau`. In this case, we will make it ten times larger. This also defines a \"fundamental frequency\", $\\omega_0 = 2\\pi/T$."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "T = 10 * tau\n",
    "omega0 = 2.0*np.pi/T\n",
    "print(\"omega0 = {:.4f}\".format(omega0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we create the [Target](../modules.rst#target.Target) object. In this case, it is an \"expectationvalue\" type, since what we want to do is to maximize the expectation value of $O = \\vert\\phi_1\\rangle \\langle\\phi_1\\vert$, i.e. the population of the first excited state."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "O = psi[1] * psi[1].dag()\n",
    "tg = target.Target(\"expectationvalue\", operator = O, T = T)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now, we must create the [pulse](../modules.rst#pulses.pulse) objects, i.e. the control functions. In this case, we have two perturbation operators, and we need two control functions. In this example, we will choose a Fourier-series parametrization for both control functions.\n",
    "\n",
    "A Fourier series must be cutoff at some maximum frequency $M\\omega_0$. We will set $M=15$, which ensures that the relevant frequency $\\omega$ is lower than the maximum frequency. Then we must give initial-guess values to the amplitudes of the Fourier series (which are the control parameters). There are $2M+1$ parameteres in each Fourier series ($M$ for the cosines, $M$ for the sines, and 1 for the zero-frequency term). We will simply set some of the parameters to some $\\kappa$ value, and zero for the rest of them. \n",
    "\n",
    "A better alternative would be to use random values, but in order to make sure that the tutorial always produces the same result, we will not use that option here."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "M = 15\n",
    "kappa = 0.1\n",
    "\n",
    "u1 = np.zeros(2*M+1)\n",
    "u2 = np.zeros(2*M+1)\n",
    "u1[3] = kappa\n",
    "u2[4] = kappa\n",
    "\n",
    "# The following code sets random numbers for the control parameters\n",
    "#a = -bound\n",
    "#b = bound\n",
    "#u1 = (b-a) * np.random.rand(2*M+1) + a\n",
    "#u2 = (b-a) * np.random.rand(2*M+1) + a"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We place the pulse objects into a list `f`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "f = []\n",
    "f.append(pulses.pulse(\"fourier\", T, u = u1))\n",
    "f.append(pulses.pulse(\"fourier\", T, u = u2))\n",
    "f0 = deepcopy(f)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We also define a couple of *zero* pulses, so that we can propagate the free system to make comparisons."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fnull = []\n",
    "fnull.append(pulses.pulse(\"fourier\", T, u = np.zeros_like(u1)))\n",
    "fnull.append(pulses.pulse(\"fourier\", T, u = np.zeros_like(u2)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`u0` will hold all the control parameters (the parameters of all the pulses together). `unull` are the parameters of the *zero* pulses."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "u0 = pulses.pulse_collection_get_parameters(f)\n",
    "unull = pulses.pulse_collection_get_parameters(fnull)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us plot to see how the pulses look like."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "ts = np.linspace(0, T, 200)\n",
    "ax.plot(ts, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax.plot(ts, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax.set_xlabel(\"t\")\n",
    "ax.set_xlim(0, T)\n",
    "ax.legend()\n",
    "\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now build the main object, of class [Qoct](../modules.rst#qoct.Qoct). Along with the Hamiltonian `H`, the target `tg`, and the set of control functions `f`, we need to pass the initial state, in our case the first excited state `psi[1]`. Also, `ntsteps`, which is the number of time steps used to discretize the time interval for the numerical integration. The higher, the more precise the calculations will be, but they will also be slower."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "rho0 = psi[1] * psi[1].dag()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ntsteps = 150\n",
    "opt = qoct.Qoct(H, T, ntsteps, tg, f, rho0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us see what the initial pulse does: we will propagate the system using the initial guess pulses, and we will plot the populations of each state as a function of time. But first, we will propagate the system with no external fields, so that we can compare.\n",
    "\n",
    "In order to the system propagation, we will use the [solve()](../modules.rst#solvers.solve) function and, in this case, the `cfmagnus4` method (see the documentation of the [solve()](../modules.rst#solvers.solve) function to learn about the propagation methods used by qocttools)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, fnull, rho0, ts)\n",
    "ps = np.zeros([ntsteps, d+1])\n",
    "for j in range(ntsteps):\n",
    "    for k in range(d):\n",
    "        ps[j, k] = qt.expect(psi[k] * psi[k].dag(), res[j])\n",
    "    ps[j, d] = qt.fidelity(rhothermal, res[j])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We plot now the populations of the three states, as the system decays from the first state. We also plot the *fidelity* of the evolving state with respect to the thermal state, to which it would eventually converge."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts, ps[:, 0], label = r\"$P_0$\")\n",
    "ax.plot(ts, ps[:, 1], label = r\"$P_1$\")\n",
    "ax.plot(ts, ps[:, 2], label = r\"$P_2$\")\n",
    "ax.plot(ts, ps[:, 3], label = r\"$P_{\\rm thermal}$\")\n",
    "ax.set_xlabel(\"t\")\n",
    "ax.set_xlim(0, T)\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We now propagate the sytemm in the presence of the initial guess pulse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, rho0, ts)\n",
    "ps = np.zeros([ntsteps, d+1])\n",
    "for j in range(ntsteps):\n",
    "    for k in range(d):\n",
    "        ps[j, k] = qt.expect(psi[k] * psi[k].dag(), res[j])\n",
    "    ps[j, d] = qt.fidelity(rhothermal, res[j])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts, ps[:, 0], label = r\"$P_0$\")\n",
    "ax.plot(ts, ps[:, 1], label = r\"$P_1$\")\n",
    "ax.plot(ts, ps[:, 2], label = r\"$P_2$\")\n",
    "ax.plot(ts, ps[:, 3], label = r\"$P_{\\rm thermal}$\")\n",
    "ax.set_xlabel(\"t\")\n",
    "ax.set_xlim(0, T)\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As one can see, the initial guess pulse does not do much: the population of the excited state decays in a similar way with respect to the zero-field case."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Before launching the optimization, one can check that the gradient is computed correctly. We can use for that purpose the [check_grad()](../modules.rst#qoct.Qoct.check_grad) method of the [Qoct](../modules.rst#qoct.Qoct) class. It computes the gradient using the QOCT formula, and using finite differences. Both numbers should match if everything is OK. Uncomment the following lines if you want to do that check."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#derqoct, dernum, error = opt.check_grad(u0)\n",
    "#print(derqoct, dernum, error)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we launch the maximization calculation, using the [maximize()](../modules.rst#qoct.Qoct.maximize) method. Note that we have added bounds on the parameters, using the `upper_bounds` and `lower_bounds` arguments. In this way, we force the algorithm to find a solution that will be contained within thouse bounds."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x, optval, result = opt.maximize(maxeval = 40,\n",
    "                                 stopval = 0.99,\n",
    "                                 verbose = True,\n",
    "                                 algorithm = nlopt.LD_SLSQP,\n",
    "                                 upper_bounds = 5.0 * kappa * np.ones_like(u0),\n",
    "                                 lower_bounds = -5.0 * kappa * np.ones_like(u0))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let us now propagate the sytem in the presence of the optimal pulse found by the algorithm, and see how it does."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "ts = np.linspace(0, T, ntsteps)\n",
    "res = solvers.solve('cfmagnus4', H, f, rho0, ts)\n",
    "ps = np.zeros([ntsteps, d])\n",
    "for k in range(d):\n",
    "    for j in range(ntsteps):\n",
    "        ps[j, k] = qt.expect(psi[k]*psi[k].dag(), res[j]).real"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "ax.plot(ts, ps[:, 0], label = r\"$P_0$\")\n",
    "ax.plot(ts, ps[:, 1], label = r\"$P_1$\")\n",
    "ax.plot(ts, ps[:, 2], label = r\"$P_2$\")\n",
    "ax.set_xlabel(\"t\")\n",
    "ax.set_xlim(0, T)\n",
    "ax.legend()\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As one can see, the population of the first excited state at the end of the propagation is higher now.\n",
    "\n",
    "Finally, we compare now in a plot the initial guess versus the optimized pulse."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots()\n",
    "\n",
    "ts = np.linspace(0, T, 200)\n",
    "ax.plot(ts, f0[0].fu(ts), label = r\"$g_x^{\\rm ini}(t)$\")\n",
    "ax.plot(ts, f0[1].fu(ts), label = r\"$g_y^{\\rm ini}(t)$\")\n",
    "ax.plot(ts, f[0].fu(ts), label = r\"$g_x^{\\rm opt}(t)$\")\n",
    "ax.plot(ts, f[1].fu(ts), label = r\"$g_y^{\\rm opt}(t)$\")\n",
    "ax.set_xlabel(\"t\")\n",
    "ax.set_xlim(0, T)\n",
    "ax.legend()\n",
    "\n",
    "plt.show()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
